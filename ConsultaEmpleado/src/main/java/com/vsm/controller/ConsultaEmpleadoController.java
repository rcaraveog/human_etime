package com.vsm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.vsm.constant.ConsultaEmpleadoConstante;
import com.vsm.dto.ConsultaAmbienteOutDto;
import com.vsm.dto.EtEmpEstDto;
import com.vsm.dto.EtParametroDto;
import com.vsm.dto.HuCatToGralDto;
import com.vsm.dto.HuRutasFileServerDto;
import com.vsm.dto.ListEtEmpEstDto;
import com.vsm.dto.OutDto;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/consultaEmpledo")
public class ConsultaEmpleadoController {
	@Autowired
	RestTemplate template;
		
    private String url1=ConsultaEmpleadoConstante.SERVICIO_DB_HUMAN;
    private String url2=ConsultaEmpleadoConstante.SERVICIO_DB_ETIME;
    private String errorCia=ConsultaEmpleadoConstante.COMPANIA_NULA;
	private String errorEmp=ConsultaEmpleadoConstante.EMPLEADO_NULO;
	private String empND=ConsultaEmpleadoConstante.EMPLEADO_NO_EXISTE;
    private String urlCAc=ConsultaEmpleadoConstante.CONSULTA_AMBIENTE_SERVICE;
	
	@GetMapping(value="/getEmpleado/{ciaNom}/{numEmp}/{fechaFoto}", produces=MediaType.APPLICATION_JSON_VALUE)
	public OutDto getEmpleado (@PathVariable("ciaNom") String ciaNom, @PathVariable("numEmp") String numEmp, @PathVariable("fechaFoto") String fechaFoto){
		String servicio = "DataBase/HuCompania/getCiaByname/{ciaNom}";
		OutDto out = new OutDto();
		
		if(ciaNom == null || ciaNom.trim().isEmpty()) {			
			out.setCodigo(errorCia);
			return out;
		}
		
		if(numEmp == null || numEmp.isEmpty() || Integer.parseInt(numEmp) <= 0) {			
			out.setCodigo(errorEmp);
			return out;
		}
		
		Map<String, String> vars = new HashMap<>();
		vars.put("ciaCve", ciaNom);
		vars.put("ciaNum", "0");
		vars.put("empNum", numEmp);	
		
		ConsultaAmbienteOutDto outCA=template.getForObject(urlCAc, ConsultaAmbienteOutDto.class, vars);
		
		if(outCA.getCodigo().equals("ET000")) {
			if(outCA.getAmbiente().equals("HU")) {
				out.setCodigo(outCA.getCodigo());
				out.setNumeroCompania(outCA.getNumCia());
				out.setNombre(outCA.getNombreEmpleado());
				
				servicio = "generic/getBeacon/{numCia}/{numEmp}";
				Map<String, Long> getBeaconParam = new HashMap<>();
				getBeaconParam.put("numCia", Long.parseLong(outCA.getNumCia()));
				getBeaconParam.put("numEmp", Long.parseLong(numEmp));
				
				ResponseEntity<String[]> responseEntity = template.getForEntity(url1+servicio, String[].class, getBeaconParam);
				List<String> responseList = Arrays.asList(responseEntity.getBody());
				if(responseList!=null) {
					List<Map<String,String>> beacons = new ArrayList<Map<String,String>>();
					for(String x : responseList) {
						if(!x.trim().isEmpty()) {
							Map<String,String> beacon = new HashMap<>();
							beacon.put("id", x);
							beacons.add(beacon);
						}
					}
					out.setBeacons(beacons);
				}
				
				Map<String, String> mapfoto = this.GetFotoHU(Long.parseLong(outCA.getNumCia()), Long.parseLong(numEmp));
				if(mapfoto != null && !mapfoto.get("foto").isEmpty() && !mapfoto.get("foto").trim().startsWith("File not found")) {
					out.setFechaFoto(mapfoto.get("fechaFoto"));
					if(!fechaFoto.equals(mapfoto.get("fechaFoto"))) {	
						out.setFoto(mapfoto.get("foto"));
					}	
				}else {
					out.setNumeroCompania(null);
					out.setNombre(null);
					out.setBeacons(null);
					out.setFechaFoto(null);
					out.setFoto(null);
					
					out.setCodigo(empND);
					return out;
				}
			}
			
			if(outCA.getAmbiente().equals("ET")) {
				out.setCodigo(outCA.getCodigo());
				out.setNumeroCompania(outCA.getNumCia());
				out.setNombre(outCA.getNombreEmpleado());
				
				servicio = "DataBase/EtEmpEst/getEstaciones/{ciaNum}/{empNum}";
				Map<String, Long> estacionesVar = new HashMap<>();
				estacionesVar.put("ciaNum", Long.parseLong(outCA.getNumCia()));
				estacionesVar.put("empNum", Long.parseLong(numEmp));
				ListEtEmpEstDto estaciones = template.getForObject(url2+servicio, ListEtEmpEstDto.class, estacionesVar);
				
				if(estaciones!=null) {
					List <EtEmpEstDto> responseList = estaciones.getEtEmpEsts();
					List<Map<String,String>> beacons = new ArrayList<Map<String,String>>();
					for(EtEmpEstDto x : responseList) {
						if(!x.getEtEstacion().getUuid().trim().isEmpty()) {
							Map<String,String> beacon = new HashMap<>();
							beacon.put("id", x.getEtEstacion().getUuid());
							beacons.add(beacon);
						}
					}
					out.setBeacons(beacons);
				}
				
				Map<String, String> mapfoto = this.GetFotoET(Long.parseLong(outCA.getNumCia()), Long.parseLong(numEmp));
				if(mapfoto != null && !mapfoto.get("foto").isEmpty() && !mapfoto.get("foto").trim().startsWith("File not found")) {					
					out.setFechaFoto(mapfoto.get("fechaFoto"));
					if(!fechaFoto.equals(mapfoto.get("fechaFoto"))) {	
						out.setFoto(mapfoto.get("foto"));
					}
				}else {
					out.setNumeroCompania(null);
					out.setNombre(null);
					out.setBeacons(null);
					out.setFechaFoto(null);
					out.setFoto(null);
					
					out.setCodigo(empND);
					return out;
				}
			}
		}else {
			out.setCodigo(outCA.getCodigo());
			return out;
		}
		
		return out;
	}
	
	public Map<String,String> GetFotoHU(long numCia, long numEmp) {
		Formatter fmt = new Formatter();
		fmt.format("%010d",numEmp);
		
		HuCatToGralDto[] respon1 = template.getForObject(url1 + "/DataBase/HuCatToGral/{parametro}",	HuCatToGralDto[].class, "FileServer");

		List<HuCatToGralDto> listGral = new ArrayList<HuCatToGralDto>();
		listGral = Arrays.asList(respon1);

		String valorAlfanumerico = null;
		String valorRuta = null;

		for (int i = 0; i < listGral.size(); i++) {
			HuCatToGralDto e = listGral.get(i);
			valorAlfanumerico = e.getValorAlfanumerico();
		}

		HuRutasFileServerDto[] respon2 = template.getForObject(url1 + "/DataBase/HuRutasFileServer/{claveRuta}",
				HuRutasFileServerDto[].class, "FOTO");

		List<HuRutasFileServerDto> listFile = new ArrayList<HuRutasFileServerDto>();
		listFile = Arrays.asList(respon2);

		for (int i = 0; i < listFile.size(); i++) {
			HuRutasFileServerDto e = listFile.get(i);
			valorRuta = e.getValorRuta();
		}

		String numcia = Long.toString(numCia);

		String filePath = valorAlfanumerico + "\\" + numcia + "\\" + valorRuta + "\\" + fmt	+ ".JPG";
		fmt.close();
		filePath = filePath.replace("\\", "/");
		filePath = filePath.replace("/", File.separator);

		File file = new File(filePath);
		String lastModified = String.valueOf(file.lastModified());		
		Map<String, String> map = new HashMap<String, String>();
		map.put("fechaFoto", lastModified);
		
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			// Reading a file from file system
			byte fileData[] = new byte[(int) file.length()];
			imageInFile.read(fileData);
			map.put("foto",Base64.getEncoder().encodeToString(fileData));
		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
			map.put("foto","File not found" + e);
			return map;
		} catch (IOException ioe) {
			System.out.println("Exception while reading the file " + ioe);
			map.put("foto","Exception while reading the file " + ioe);
			return map;
		}
		
		return map;
	}
	
	public Map<String,String> GetFotoET(long numCia, long numEmp) {
		Formatter fmtCia = new Formatter();
		fmtCia.format("%05d",numCia);
		
		Formatter fmtEmp = new Formatter();
		fmtEmp.format("%010d",numEmp);
		
		EtParametroDto[] respon1 = template.getForObject(url2 + "/DataBase/EtParmetros/{parametro}", EtParametroDto[].class, "FileHumaneTime");

		List<EtParametroDto> listGral = new ArrayList<EtParametroDto>();
		listGral = Arrays.asList(respon1);

		String valorAlfanumerico = null;

		for (int i = 0; i < listGral.size(); i++) {
			EtParametroDto e = listGral.get(i);
			valorAlfanumerico = e.getValor();
		}

		String filePath = valorAlfanumerico + "\\" + fmtCia + "\\" + fmtEmp	+ ".JPG";
		fmtCia.close();
		fmtEmp.close();
		filePath = filePath.replace("\\", "/");
		filePath = filePath.replace("/", File.separator);

		File file = new File(filePath);
		String lastModified = String.valueOf(file.lastModified());
		Map<String, String> map = new HashMap<String, String>();
		map.put("fechaFoto", lastModified);
		
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			// Reading a file from file system
			byte fileData[] = new byte[(int) file.length()];
			imageInFile.read(fileData);
			map.put("foto",Base64.getEncoder().encodeToString(fileData));
		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
			map.put("foto","File not found" + e);
			return map;
		} catch (IOException ioe) {
			System.out.println("Exception while reading the file " + ioe);
			map.put("foto","Exception while reading the file " + ioe);
			return map;
		}
		
		return map;
	}
}