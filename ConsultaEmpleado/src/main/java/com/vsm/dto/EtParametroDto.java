package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtParametroDto{
	private long idParametro;
	private String parametro;
	private String valor;
	private String estatus;
}