package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InDto {
	String compania;
	String empleado;
	String fechaFoto;
}