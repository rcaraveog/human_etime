package com.vsm.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuCatToGralDto{
	private String parametro;
	private LocalDateTime fechaMov;
	private String status;
	private String userId;
	private String valorAlfanumerico;
	private BigDecimal valorNumerico;
}