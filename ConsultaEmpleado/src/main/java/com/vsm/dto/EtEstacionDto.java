package com.vsm.dto;

import java.math.BigDecimal;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtEstacionDto {
	private long idEstacion;
	private String cveUnidad;
	private String estatus;
	private BigDecimal idZona;
	private BigDecimal latitud;
	private BigDecimal longitud;
	private String nombre;
	private BigDecimal rango;
	private String uuid;
}