package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtEmpEstPKDto {
	private long idCia;
	private long idEmpleado;
	private long idEstacion;
}