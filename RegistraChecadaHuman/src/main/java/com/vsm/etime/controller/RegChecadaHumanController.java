package com.vsm.etime.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.ChecadaEntradaInDto;
import com.vsm.etime.dto.ChecadaEntradaOutDto;
import com.vsm.etime.dto.ChecadaSalidaInDto;
import com.vsm.etime.dto.ChecadaSalidaOutDto;
import com.vsm.etime.dto.CompareRequest;
import com.vsm.etime.dto.CompareResponse;
import com.vsm.etime.dto.HuClienteConnectDto;
import com.vsm.etime.dto.MrDto;
import com.vsm.etime.dto.RequestHumaneTime;
import com.vsm.etime.dto.ResponseHumaneTime;
import com.vsm.etime.dto.ValidaEstacionDto;
import com.vsm.etime.dto.ValidaLicenciaDto;
import com.vsm.etime.service.ChecarEntrada;
import com.vsm.etime.service.ChecarSalida;
import com.vsm.etime.service.GetFotoModel;
import com.vsm.etime.service.ValidaEstacion;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/HumanVinculado")
@Api(tags = "RegChecadaHuman")
public class RegChecadaHumanController {
	private static final Logger LOG = Logger.getLogger(RegChecadaHumanController.class.getName());
		
	@Autowired
	RestTemplate template;	
	
	@Autowired
	ValidaEstacion valEstService;
	
	@Autowired
	GetFotoModel fotoService;
	
	@Autowired 
	ChecarEntrada entradaService;
	
	@Autowired 
	ChecarSalida salidaService;
	
	@ApiOperation(value = "Registro de Checadas Human eLand Vinculado")
	@PostMapping(value="registraChecada",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseHumaneTime registraChecada(@RequestBody RequestHumaneTime in) {		
		ResponseHumaneTime out = new ResponseHumaneTime();
		Map<String, String> validaLicParam = new HashMap<>();		
		validaLicParam.put("tipoLic", "T");
		validaLicParam.put("ciaNum", String.valueOf(in.getNumCia()));
		String servicio = RegChecadaHumanConstante.VALIDA_LICENCIA_SERVICE;
				
		//validacion de licencia BEGIN
		ValidaLicenciaDto cia = template.getForObject(servicio, ValidaLicenciaDto.class, validaLicParam);
		if(!cia.getCodigo().equalsIgnoreCase(RegChecadaHumanConstante.SUCCES)) {
			LOG.info("Codigo error en validar Licencia: " + cia.getCodigo() + " - Parametros: " + validaLicParam.toString());
			out.setCodigo(cia.getCodigo());
			return out;
		}
		//validacion de licencia END

		//validacion de saldo BEGIN
		LocalDate ld = LocalDate.now();
		String año = String.valueOf(ld.getYear());
		String fechaIni = año + "-01-01";
		String fechaFin = año + "-12-31";
		String fecha = in.getFechaChecada().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		Long saldo = 0L;
		
		Map<String, String> numChecadasParam = new HashMap<>();		
		numChecadasParam.put("numCia", String.valueOf(in.getNumCia()));
		numChecadasParam.put("fehcaIni", fechaIni);
		numChecadasParam.put("fechaFin", fechaFin);
		numChecadasParam.put("numEmp", String.valueOf(in.getNumEmp()));
		numChecadasParam.put("fecha", fecha);
		servicio = RegChecadaHumanConstante.NUMERO_CHECADAS_SERVICE;
		
		/**Long checadas = template.getForObject(servicio, Long.class, numChecadasParam);
		if(checadas<0) {
			LOG.info("Sin saldo en RN: " + numChecadasParam.toString());
			out.setCodigo(RegChecadaHumanConstante.ERROR_SALDO);
			return out;
		}
		
		for(HuClienteConnectDto x:cia.getHuClienteConnectDto()) {
			saldo = saldo + x.getChecadaDias().longValue();
		}
		
		if(checadas>saldo) {
			LOG.info("checadas: " + checadas + "  --  saldo: " + saldo);
			out.setCodigo(RegChecadaHumanConstante.ERROR_SALDO);
			return out;
		}**/
		//validacion de saldo END
		
		//validacion maestro de registro BEGIN
		Map<String, String> mrParam = new HashMap<>();		
		mrParam.put("numCia", String.valueOf(in.getNumCia()));
		mrParam.put("numEmp", String.valueOf(in.getNumEmp()));
		servicio = RegChecadaHumanConstante.MR_SERVICE;
		
		MrDto[] mrDto = template.getForObject(servicio, MrDto[].class, mrParam);
		if(mrDto==null) {
			LOG.info("Sin coincidencia en MR: " + mrParam.toString());
			out.setCodigo(RegChecadaHumanConstante.ERROR_MR);
			return out;
		}
		
		List<MrDto> mrsDto = new ArrayList<MrDto>();
		mrsDto = Arrays.asList(mrDto);
		//validacion maestro de registro END		
		
		//validacion estaciones BEGIN
		String beacon = " ";
		String[] beacons = in.getBeacon();
		
		if(beacons!=null && beacons.length>0)beacon = beacons[0]; 
		ValidaEstacionDto estacion = valEstService.valEstacion( String.valueOf(in.getNumCia()), String.valueOf(in.getNumEmp()), String.valueOf(in.getPrioridad()), 
																String.valueOf(in.getGeolocalizacion().getLatitud()), String.valueOf(in.getGeolocalizacion().getLongitud()), beacon);
		
		if(estacion.getCodigo()!=RegChecadaHumanConstante.SUCCES) {
			LOG.info("Sin coincidencia con alguna estacion");
			out.setCodigo(estacion.getCodigo());
			return out;
		}
		//validacion estaciones END
		
		//validacion foto BEGIN
		Map<String, String> mapfoto = fotoService.GetFoto(in.getNumCia(), in.getNumEmp());
		servicio = RegChecadaHumanConstante.COMPARA_FOTO_HUMAN_SERVICE;
		CompareRequest req =  new CompareRequest();
		req.setCompany(cia.getHuCompaniaDto().getClaveCompania());
		req.setEmployeeNo(String.valueOf(in.getNumEmp()));
		req.setModel(mapfoto.get("foto"));
		req.setPhoto(in.getFoto());
		req.setHash(fotoService.encryptPassword(mapfoto.get("foto")));		
		CompareResponse res = template.postForObject(servicio, req, CompareResponse.class);
		
		/**if(!res.isRes()){	
			LOG.info("La imagen no coincide con el modelo");
			out.setCodigo(RegChecadaHumanConstante.ERROR_COMPARA_FOTO);
			return out;
		}**/
		//validacion foto END
		
		if(in.getTipoChecada().equalsIgnoreCase("E")) {
			ChecadaEntradaInDto entradaIn = new ChecadaEntradaInDto();
			entradaIn.setEstacion(estacion.getTa155().getId().getTa155Estacion());
			entradaIn.setHoraChecada(in.getFechaChecada());
			entradaIn.setMrDto(mrsDto.get(0));
			entradaIn.setNumCia(in.getNumCia());
			entradaIn.setNumEmp(in.getNumEmp());
			entradaIn.setTipoChecada(in.getTipoChecada());
			ChecadaEntradaOutDto entradaOut = entradaService.entrada(entradaIn);
			if(!entradaOut.getCodigo().equalsIgnoreCase(RegChecadaHumanConstante.SUCCES)) {				
				out.setCodigo(entradaOut.getCodigo());
				return out;
			}
		}
		
		if(in.getTipoChecada().equalsIgnoreCase("S")) {
			ChecadaSalidaInDto salidaIn = new ChecadaSalidaInDto();
			salidaIn.setEstacion(in.getBeacon()[0]);
			salidaIn.setHoraChecada(in.getFechaChecada());
			salidaIn.setMrDto(mrsDto.get(0));
			salidaIn.setNumCia(in.getNumCia());
			salidaIn.setNumEmp(in.getNumEmp());
			salidaIn.setTipoChecada(in.getTipoChecada());
			ChecadaSalidaOutDto salidaOut = salidaService.salida(salidaIn);
			if(!salidaOut.getCodigo().equalsIgnoreCase(RegChecadaHumanConstante.SUCCES)) {				
				out.setCodigo(salidaOut.getCodigo());
				return out;
			}
		}
		out.setCodigo(RegChecadaHumanConstante.SUCCES);
		out.setEstacion(estacion.getTa155().getId().getTa155Estacion());
		return out;
	}	
}