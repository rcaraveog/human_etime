package com.vsm.etime.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrDto {
	private PrPKDto id;
	private BigDecimal prConcepto;
	private LocalDateTime prFechaCap;
	private LocalDateTime prHoraCap;
	private BigDecimal prHrsPer1;
	private BigDecimal prHrsPer2;
	private BigDecimal prMinRetardo;
	private String prObservacion;
	private LocalDateTime prPermHhFin;
	private String prSts;
	private String prUsuario;
}