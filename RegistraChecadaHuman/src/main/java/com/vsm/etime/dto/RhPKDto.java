package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RhPKDto {
	private long rhCia;
	private long rhNumEmp;
	private LocalDateTime rhFecIni;
}