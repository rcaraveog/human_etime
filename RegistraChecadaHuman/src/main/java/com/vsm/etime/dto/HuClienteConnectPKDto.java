package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuClienteConnectPKDto {
	private long numCia;
	private String tipoConexion;
	private String vigencia;
	private LocalDateTime fechaIni;
	private String siglasCliente;
	private String pais;
}