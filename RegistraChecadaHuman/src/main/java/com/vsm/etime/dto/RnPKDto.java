package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RnPKDto {
	private long rnCia;
	private long rnNumEmp;
	private LocalDateTime rnFecha;
	private long rnSecuencia;
}