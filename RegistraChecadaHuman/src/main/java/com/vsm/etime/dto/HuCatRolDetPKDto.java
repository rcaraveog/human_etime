package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuCatRolDetPKDto {
	private long numCia;
	private long claveRol;
	private long claveTurno;
	private LocalDateTime fechaIni;
}