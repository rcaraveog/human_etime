package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuCatRolDetDto {
	private HuCatRolDetPKDto id;
	private String diasDescanso;
	private LocalDateTime fechaFin;
	private LocalDateTime fechaMov;
	private String secuenciaInicial;
	private String status;
	private String userId;
}