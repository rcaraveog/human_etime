package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Ta140PKDto {
	private long ta140Cia;
	private long ta140CveTurno;
}