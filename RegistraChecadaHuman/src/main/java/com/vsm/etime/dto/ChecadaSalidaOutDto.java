package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChecadaSalidaOutDto {
	String codigo;
	String estacion;
}