package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuRutasFileServerDto {
	private String claveRuta;
	private String descripcion;
	private LocalDateTime fechaMov;
	private String status;
	private String usuario;
	private String valorRuta;
}