package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChecadaSalidaInDto {
	long numCia;
	long numEmp;
	LocalDateTime horaChecada;
	String tipoChecada;
	String Estacion;
	MrDto mrDto;
}