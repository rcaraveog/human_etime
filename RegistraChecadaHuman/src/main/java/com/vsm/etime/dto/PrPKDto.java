package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrPKDto {
	private long prCia;
	private long prNumEmp;
	private LocalDateTime prFecha;
	private LocalDateTime prPermHhIni;
}