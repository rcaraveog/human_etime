package com.vsm.etime.dto;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuRelojRegTxtPKDto {
	private long cia;
	private long numEmp;
	private LocalDateTime fecha;
	private LocalDateTime hora;
}