package com.vsm.etime.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuRelojRegTxtDto {
	private HuRelojRegTxtPKDto id;
	private BigDecimal comida;
	private String cveChecada;
	private String estacion;
	private LocalDateTime fechaMov;
	private String motChecada;
	private String status;
	private String userId;
}