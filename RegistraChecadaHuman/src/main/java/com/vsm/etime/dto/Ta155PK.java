package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Ta155PK {
	private long ta155Cia;
	private String ta155Estacion;
}