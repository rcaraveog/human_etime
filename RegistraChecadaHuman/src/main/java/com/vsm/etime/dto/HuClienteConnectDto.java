package com.vsm.etime.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuClienteConnectDto {
	private HuClienteConnectPKDto id;
	private BigDecimal año;
	private BigDecimal checadaDias;
	private BigDecimal conexiones;
	private BigDecimal conexionesGpoA;
	private BigDecimal conexionesGpoB;
	private BigDecimal conexionesGpoC;
	private BigDecimal conexionesGpoD;
	private BigDecimal conexionesGpoE;
	private BigDecimal conexionesGpoF;
	private BigDecimal conexionesGpoG;
	private BigDecimal conexionesGpoH;
	private BigDecimal conexionesGpoI;
	private BigDecimal conexionesGpoJ;
	private LocalDateTime fechaFin;
	private LocalDateTime fechaMov;
	private String generaCfdi;
	private BigDecimal publicacionesAnuales;
	private String status;
	private String userId;
}