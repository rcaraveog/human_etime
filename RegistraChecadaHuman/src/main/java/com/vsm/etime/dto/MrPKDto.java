package com.vsm.etime.dto;

import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MrPKDto {
	private long mrCia;
	private long mrNumEmp;
	private LocalDate mrFechaApli;
}