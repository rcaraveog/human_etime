package com.vsm.etime.constant;

public interface RegChecadaHumanConstante {
	public static final String SUCCES = System.getenv().get("GET_COD_ET000");
	public static final String SUCCES_RETARDO = System.getenv().get("GET_COD_CT000");
	public static final String VALIDA_LICENCIA_SERVICE = System.getenv().get("GET_VALIDA_LIC");
	public static final String MR_SERVICE = System.getenv().get("GET_MR");
	public static final String NUMERO_CHECADAS_SERVICE = System.getenv().get("GET_NUMERO_CHECADAS");
	public static final String ESTACIONES_SERVICE = System.getenv().get("GET_ESTACIONES");
	public static final String PARAMETRO_HUMAN_SERVICE = System.getenv().get("GET_PARAMETRO_HUMAN");
	public static final String COMPARA_FOTO_HUMAN_SERVICE = System.getenv().get("COMPARA_FOTO_REST_SERVICE");
	public static final String RN_FALTA_SERVICE = System.getenv().get("GET_RN_FALTA");
	public static final String DELETE_RN_FALTA_SERVICE = System.getenv().get("DELETE_RN_FALTA");
	public static final String RELOJREGTXT_SERVICE = System.getenv().get("GET_RELOJREGTXT");
	public static final String RN_LAST_MOVE_SERVICE = System.getenv().get("GET_RN_LAST_MOVE");
	public static final String RN_LAST_MOVE_BY_DATE_SERVICE = System.getenv().get("GET_RN_LAST_MOVE_BY_DATE");
	public static final String HUCATCTGRAL_SERVICE = System.getenv().get("GET_HUCATTOGRAL");
	public static final String PR_RETARDO_SERVICE = System.getenv().get("GET_PR_RETARDO");
	public static final String RH_SERVICE = System.getenv().get("GET_RH");
	public static final String HU_CAT_ROL_DET_SERVICE = System.getenv().get("GET_HU_CAT_ROL_DET");
	public static final String TA140_SERVICE = System.getenv().get("GET_TA140");
	public static final String PR_SAVE_SERVICE = System.getenv().get("GET_PR_SAVE_SERVICE");
	public static final String RN_SAVE_SERVICE = System.getenv().get("GET_RN_SAVE_SERVICE");
	public static final String HURELOJREGTXT_SAVE_SERVICE = System.getenv().get("GET_HURELOJREGTXT_SAVE_SERVICE");
	public static final String UPDATE_RN_FALTA_SERVICE = System.getenv().get("UPDATE_RN_SALIDA");
	
	//Errores Constantes
	public static final String ERROR_GENERAL = System.getenv().get("GET_COD_ERROR_EG000");
	public static final String ERROR_MR = System.getenv().get("GET_COD_ERROR_ET031");
	public static final String ERROR_SALDO = System.getenv().get("GET_COD_ERROR_ET240");
	public static final String ESTACION_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET506");
	public static final String P1_MATCH_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET029");
	public static final String P2_MATCH_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET030");
	public static final String P3_MATCH_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET507");
	public static final String ERROR_COMPARA_FOTO = System.getenv().get("GET_COD_ERROR_ET032");
	public static final String ERROR_BASE_DATOS = System.getenv().get("GET_COD_ERROR_WS022");
	public static final String ERROR_CHECADA_DUPLICADA = System.getenv().get("GET_COD_ERROR_ET041");
	public static final String SALIDA_MAYOR_CHECADA = System.getenv().get("GET_COD_ERROR_ET042");
	public static final String ERROR_ENTRADA_DUPLICADA = System.getenv().get("GET_COD_ERROR_ET033");
	public static final String ERROR_RN_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET003");
	public static final String ERROR_CLAVE_TURNO_NULL = System.getenv().get("GET_COD_ERROR_CT002");
	public static final String ERROR_TURNO_NULL = System.getenv().get("GET_COD_ERROR_CT003");
	public static final String ERROR_RETARDO_FALTA = System.getenv().get("GET_COD_ERROR_ET522");
	public static final String ERROR_SALIDA = System.getenv().get("GET_COD_ERROR_ET040");
	public static final String ENTRADA_MAYOR_CHECADA = System.getenv().get("GET_COD_ERROR_ET045");
}