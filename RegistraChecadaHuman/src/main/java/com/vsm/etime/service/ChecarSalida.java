package com.vsm.etime.service;

import com.vsm.etime.dto.ChecadaSalidaInDto;
import com.vsm.etime.dto.ChecadaSalidaOutDto;

public interface ChecarSalida {
	ChecadaSalidaOutDto salida(ChecadaSalidaInDto tipoSalida); 
}