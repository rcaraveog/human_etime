package com.vsm.etime.service.impl;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.HuCatCtGralDto;
import com.vsm.etime.dto.HuCatRolDetDto;
import com.vsm.etime.dto.MrDto;
import com.vsm.etime.dto.PrDto;
import com.vsm.etime.dto.PrPKDto;
import com.vsm.etime.dto.RhDto;
import com.vsm.etime.dto.RnDto;
import com.vsm.etime.dto.SetRetardoOutDto;
import com.vsm.etime.dto.Ta140Dto;
import com.vsm.etime.service.SetRetardo;

@Service("setRetardoService")
public class SetRetardoImpl implements SetRetardo {
	@Autowired
	RestTemplate template;
	
	@Override
	public SetRetardoOutDto setRetardo(Long numCia, Long numEmp, LocalDateTime horaChecada, RnDto lastMove, MrDto mr, Long maxRnSec, HuCatCtGralDto huCatCtGral) {
		// TODO Auto-generated method stub
		BigDecimal claveTurno = mr.getMrCveTurno();
		BigDecimal claveHorario = mr.getMrCveHor();
		SetRetardoOutDto out = new SetRetardoOutDto();
		String fechaSinHora = horaChecada.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		
		String servicio = RegChecadaHumanConstante.RH_SERVICE;
		Map<String, String> rhParam = new HashMap<>();
		rhParam.put("numCia", String.valueOf(numCia));
		rhParam.put("numEmp", String.valueOf(numEmp));
		rhParam.put("fecha", horaChecada.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));		
		RhDto[] rhlist = template.getForObject(servicio, RhDto[].class, rhParam);
		
		if(rhlist!=null) {
			List<RhDto> rhs = new ArrayList<RhDto>();
			rhs = Arrays.asList(rhlist);
			claveTurno = rhs.get(0).getRhCveTurno();
			claveHorario = rhs.get(0).getRhCveHor();
		}
		
		if(claveTurno.equals(BigDecimal.ZERO) && claveHorario.longValue()>0) {
			servicio = RegChecadaHumanConstante.HU_CAT_ROL_DET_SERVICE;
			Map<String, String> huCatRolDetParam = new HashMap<>();
			huCatRolDetParam.put("numCia", String.valueOf(numCia));
			huCatRolDetParam.put("cveRol", String.valueOf(claveHorario));
			huCatRolDetParam.put("fechaIni", horaChecada.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));		
			HuCatRolDetDto[] huCatRolDetlist = template.getForObject(servicio, HuCatRolDetDto[].class, huCatRolDetParam);
			
			if(huCatRolDetlist!=null) {
				List<HuCatRolDetDto> huCatRolDets = new ArrayList<HuCatRolDetDto>();
				huCatRolDets = Arrays.asList(huCatRolDetlist);
				claveTurno = new BigDecimal(huCatRolDets.get(0).getId().getClaveTurno());
			}
			
			if(claveTurno.longValue()<=0){
				out.setCodigo(RegChecadaHumanConstante.ERROR_CLAVE_TURNO_NULL);
				out.setMsj("No esta definida la clave del turno");
				return out;
			}
		}
			
		servicio = RegChecadaHumanConstante.TA140_SERVICE;
		Map<String, String> ta140Param = new HashMap<>();
		ta140Param.put("numCia", String.valueOf(numCia));
		ta140Param.put("claveTurno", String.valueOf(claveTurno));
		Ta140Dto ta140 = template.getForObject(servicio, Ta140Dto.class, ta140Param);
		
		if(ta140==null) {
			out.setCodigo(RegChecadaHumanConstante.ERROR_TURNO_NULL);
			out.setMsj("No se encuentra el turno");
			return out;
		}
		
		int diaSemana = horaChecada.getDayOfWeek().getValue();
		String entradaToleranciaStr = "";
		String replace = horaChecada.toString().substring(0,10) + " ";
		
		switch (diaSemana) {
			case 1: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Lun().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Lun().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}
				break;
			case 2: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Mar().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Mar().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}
				break;	
			case 3: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Mie().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Mie().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01TT", replace);
				}
				break;
			case 4: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Jue().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Jue().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}
				break;
			case 5: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Vie().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Vie().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}
				break;
			case 6: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Sab().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Sab().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}
				break;
			case 7: 
				if(maxRnSec==1) {
					entradaToleranciaStr = ta140.getTa140HoraE1Dom().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}else {
					entradaToleranciaStr = ta140.getTa140HoraE2Dom().plusMinutes(ta140.getTa140MinTol().longValue()).toString();
					entradaToleranciaStr = entradaToleranciaStr.replace("1900-01-01T", replace);
				}
				break;
		}
		
		if(horaChecada.isAfter(LocalDateTime.parse(entradaToleranciaStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")))) {
			long minutesRet = Duration.between(LocalDateTime.parse(entradaToleranciaStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")), horaChecada).toMinutes();
			servicio = RegChecadaHumanConstante.PR_SAVE_SERVICE;
			PrDto regPr = new PrDto();
			PrPKDto prPK = new PrPKDto();
			prPK.setPrCia(numCia);
			regPr.setPrConcepto(BigDecimal.ZERO);
			prPK.setPrFecha(LocalDate.parse(fechaSinHora, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay());
			regPr.setPrFechaCap(LocalDate.now().atStartOfDay());
			regPr.setPrHoraCap(LocalDateTime.now());
			regPr.setPrHrsPer1(BigDecimal.ZERO);
			regPr.setPrHrsPer2(BigDecimal.ZERO);
			regPr.setPrMinRetardo(new BigDecimal(minutesRet));
			prPK.setPrNumEmp(numEmp);
			regPr.setPrObservacion(" ");
			regPr.setPrPermHhFin(horaChecada);
			prPK.setPrPermHhIni(LocalDateTime.parse(entradaToleranciaStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
			regPr.setPrSts("A");
			regPr.setPrUsuario("Etime");	
			regPr.setId(prPK);
			String guardarPr = template.postForObject(servicio, regPr, String.class);
			
			if(!guardarPr.equalsIgnoreCase("Registro creado")) {
				out.setCodigo(RegChecadaHumanConstante.ERROR_GENERAL);
				out.setMsj("Error al inserter Pr");
				return out;
			}
			
			if(huCatCtGral.getChecarRetardo().equalsIgnoreCase("N")) {
				if(minutesRet > huCatCtGral.getMinChecAntesEntrada().longValue()) {
					out.setCodigo(RegChecadaHumanConstante.ERROR_RETARDO_FALTA);
					out.setMsj("Minutos mayor a lo permitido en huCatCtGral");
					return out;
				}
			}
		}else {
			out.setCodigo(RegChecadaHumanConstante.SUCCES_RETARDO);
			return out;
		}
		out.setCodigo(RegChecadaHumanConstante.SUCCES_RETARDO);
		return out;
	}
}