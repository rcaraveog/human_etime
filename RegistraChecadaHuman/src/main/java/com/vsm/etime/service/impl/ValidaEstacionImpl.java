package com.vsm.etime.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.Ta155;
import com.vsm.etime.dto.ValidaEstacionDto;
import com.vsm.etime.service.ValidaEstacion;

@Service("validaEstacionService")
public class ValidaEstacionImpl implements ValidaEstacion {
	private static final Logger LOG = Logger.getLogger(ValidaEstacionImpl.class.getName());
	
	@Autowired
	RestTemplate template;
	
	@Override
	public ValidaEstacionDto valEstacion(String numCia, String numEmp, String prioridad,
										  String latitud, String longitud, String beacon) {
		ValidaEstacionDto out = new ValidaEstacionDto();
		Map<String, String> estacionesParam = new HashMap<>();		
		estacionesParam.put("numCia", numCia);
		estacionesParam.put("numEmp", numEmp);
		String servicio = RegChecadaHumanConstante.ESTACIONES_SERVICE;
		
		Ta155[] ta155 = template.getForObject(servicio, Ta155[].class, estacionesParam);
		if(ta155==null) {
			LOG.info("Sin coincidencia en Ta155: " + estacionesParam.toString());
			out.setCodigo(RegChecadaHumanConstante.ESTACION_NO_EXISTE);
			return out;
		}
		List<Ta155> estaciones = Arrays.asList(ta155);
		double  metros = 0;
				
		if(estaciones.size()>0) {			
			if(prioridad.equalsIgnoreCase("2")||prioridad.equalsIgnoreCase("3")) {				
				for(Ta155 x : estaciones) {
					try{     
				        double radioTierra = 6371;//en kilómetros
				        double  Latitud01  =x.getTa155CoordX().doubleValue();   
				        double  Latitud02  =Double.parseDouble(latitud);   
				        double  Longitud01 =x.getTa155CoordY().doubleValue();    
				        double  Longitud02 =Double.parseDouble(longitud);
				        double dLat = Math.toRadians(Latitud02 - Latitud01);      
				        double dLng = Math.toRadians(Longitud02 - Longitud01);
				        double sindLat = Math.sin(dLat / 2);      
				        double sindLng = Math.sin(dLng / 2);      
				        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(Latitud01)) * Math.cos(Math.toRadians(Latitud02)); 
				        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));      
				        double distancia = radioTierra * va2;      
				        
				        metros = 1000 * distancia;        
				     
					}catch ( Exception ex ){           
						out.setCodigo(RegChecadaHumanConstante.ERROR_GENERAL);
						return out;           
					}
	
					double rango = x.getTa155Radio().doubleValue();
					if(rango>=metros) {
						out.setCodigo(RegChecadaHumanConstante.SUCCES);
						out.setTa155(x);
						return out;
					}
				}
			}
			
			if(prioridad.equalsIgnoreCase("1")||prioridad.equalsIgnoreCase("3")) {
				for(Ta155 x : estaciones) {
					if(x.getTa155Beacon().equalsIgnoreCase(beacon)) {
						out.setCodigo(RegChecadaHumanConstante.SUCCES);
						out.setTa155(x);
						return out;
					}
				}
			}
		}else {
			out.setCodigo(RegChecadaHumanConstante.ESTACION_NO_EXISTE);
			return out;
		}
		
		if(prioridad.equalsIgnoreCase("1")) {
			out.setCodigo(RegChecadaHumanConstante.P1_MATCH_NO_EXISTE);
		}
		
		if(prioridad.equalsIgnoreCase("2")) {
			out.setCodigo(RegChecadaHumanConstante.P2_MATCH_NO_EXISTE);
		}
		
		if(prioridad.equalsIgnoreCase("3")) {
			out.setCodigo(RegChecadaHumanConstante.P3_MATCH_NO_EXISTE);
		}		
		return out;		
	}
}