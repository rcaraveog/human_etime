package com.vsm.etime.service;

import com.vsm.etime.dto.ChecadaEntradaInDto;
import com.vsm.etime.dto.ChecadaEntradaOutDto;

public interface ChecarEntrada {
	ChecadaEntradaOutDto entrada(ChecadaEntradaInDto tipoEntrada); 
}