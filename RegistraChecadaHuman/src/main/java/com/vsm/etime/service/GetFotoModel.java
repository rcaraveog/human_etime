package com.vsm.etime.service;

import java.util.Map;

public interface GetFotoModel {
	Map<String,String> GetFoto(long numCia, long numEmp);
	String encryptPassword(String password); 
}