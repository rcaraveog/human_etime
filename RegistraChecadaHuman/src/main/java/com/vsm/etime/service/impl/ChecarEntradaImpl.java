package com.vsm.etime.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.ChecadaEntradaInDto;
import com.vsm.etime.dto.ChecadaEntradaOutDto;
import com.vsm.etime.dto.HuCatCtGralDto;
import com.vsm.etime.dto.HuRelojRegTxtDto;
import com.vsm.etime.dto.HuRelojRegTxtPKDto;
import com.vsm.etime.dto.PrDto;
import com.vsm.etime.dto.RnDto;
import com.vsm.etime.dto.RnPKDto;
import com.vsm.etime.dto.SetFaltaOutDto;
import com.vsm.etime.dto.SetRetardoOutDto;
import com.vsm.etime.service.ChecarEntrada;
import com.vsm.etime.service.SetFalta;
import com.vsm.etime.service.SetRetardo;

@Service("checarEntradaService")
public class ChecarEntradaImpl implements ChecarEntrada {
	private static final Logger LOG = Logger.getLogger(ChecarEntradaImpl.class.getName());
	
	@Autowired
	SetFalta service;
	
	@Autowired
	SetRetardo serviceRetardo;
	
	@Autowired
	RestTemplate template;
	
	@Override
	public ChecadaEntradaOutDto entrada(ChecadaEntradaInDto in) {
		// TODO Auto-generated method stub
		ChecadaEntradaOutDto out = new ChecadaEntradaOutDto();
		String fechaPasada = in.getHoraChecada().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String fechaUltima = "";
		boolean calculoFlag = false;
		Long maxRnSec = 1L;
				
		Map<String, String> rnFaltaParam = new HashMap<>();
		rnFaltaParam.put("numCia", String.valueOf(in.getNumCia()));
		rnFaltaParam.put("numEmp", String.valueOf(in.getNumEmp()));
		rnFaltaParam.put("date", fechaPasada);
		String servicio = RegChecadaHumanConstante.RN_FALTA_SERVICE;		
		RnDto falta = template.getForObject(servicio, RnDto.class, rnFaltaParam);		
		
		if(falta!=null) {
			Map<String, String> rnDeleteParam = new HashMap<>();
			rnDeleteParam.put("numCia", String.valueOf(in.getNumCia()));
			rnDeleteParam.put("numEmp", String.valueOf(in.getNumEmp()));
			rnDeleteParam.put("date", fechaPasada);
			servicio = RegChecadaHumanConstante.DELETE_RN_FALTA_SERVICE;			
			String delResult = template.getForObject(servicio, String.class, rnDeleteParam);
			
			if(!delResult.equalsIgnoreCase("registro eliminado")) {
				out.setCodigo(RegChecadaHumanConstante.ERROR_BASE_DATOS);
				return out;
			}
			calculoFlag=true;			
		}

		servicio = RegChecadaHumanConstante.RELOJREGTXT_SERVICE;
		Map<String, String> relojRegTxtParam = new HashMap<>();
		relojRegTxtParam.put("numCia", String.valueOf(in.getNumCia()));
		relojRegTxtParam.put("numEmp", String.valueOf(in.getNumEmp()));
		relojRegTxtParam.put("fecha", in.getHoraChecada().toString().substring(0,10));
		relojRegTxtParam.put("hora", in.getHoraChecada().toString().replace("T", " ").substring(0,19));		
		HuRelojRegTxtDto relojRegTxt = template.getForObject(servicio, HuRelojRegTxtDto.class, relojRegTxtParam);		
		
		if(relojRegTxt!=null) {
			LOG.info("existe registro en HU_RELOJ_REG_TXT parametros: " + relojRegTxtParam.toString());
			out.setCodigo(RegChecadaHumanConstante.ERROR_CHECADA_DUPLICADA);
			return out;
		}
		
		Map<String, String> rnLastMoveParam = new HashMap<>();
		rnLastMoveParam.put("numCia", String.valueOf(in.getNumCia()));
		rnLastMoveParam.put("numEmp", String.valueOf(in.getNumEmp()));
		servicio = RegChecadaHumanConstante.RN_LAST_MOVE_SERVICE;		
		RnDto lastMove = template.getForObject(servicio, RnDto.class, rnLastMoveParam);
		
		if(lastMove!=null) {
			fechaUltima = lastMove.getId().getRnFecha().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}	
		
		Map<String, String> rnLastMoveFechaParam = new HashMap<>();
		rnLastMoveFechaParam.put("numCia", String.valueOf(in.getNumCia()));
		rnLastMoveFechaParam.put("numEmp", String.valueOf(in.getNumEmp()));
		rnLastMoveFechaParam.put("date", fechaPasada);
		servicio = RegChecadaHumanConstante.RN_LAST_MOVE_BY_DATE_SERVICE;		
		RnDto lastMoveByDate = template.getForObject(servicio, RnDto.class, rnLastMoveFechaParam);
		
		if(lastMoveByDate!=null) {
			maxRnSec=maxRnSec+lastMoveByDate.getId().getRnSecuencia();
		}
		
		if(fechaPasada.equalsIgnoreCase(fechaUltima) && lastMove.getRnHorSal()!=null) {
			if(lastMove.getRnHorSal().isAfter(in.getHoraChecada())) {
				LOG.info("la ultima fecha de salida: " + lastMove.getRnHorSal().toString() + " es mayor a la hora de checada");
				out.setCodigo(RegChecadaHumanConstante.SALIDA_MAYOR_CHECADA);
				return out;
			}
		}
		
		if(fechaPasada.equalsIgnoreCase(fechaUltima) && !calculoFlag) {
			if(in.getHoraChecada().isBefore(lastMove.getId().getRnFecha())) {
				if(falta!=null) {
					if(falta.getRnHorSal()!=null && falta.getRnHorSal().isBefore(in.getHoraChecada())) {
						LOG.info("la ultima fecha de salida: " + lastMove.getRnHorSal().toString() + " es mayor a la hora de checada");
						out.setCodigo(RegChecadaHumanConstante.SALIDA_MAYOR_CHECADA);
						return out;
					}
					
					if(falta.getRnHorSal()==null) {
						LOG.info("la ultima fecha no tiene checada de salida");
						out.setCodigo(RegChecadaHumanConstante.ERROR_ENTRADA_DUPLICADA);
						return out;
					}
				}
			}else {
				SetFaltaOutDto setFalta = service.setFalta(in.getNumCia(), in.getNumEmp(), in.getHoraChecada(), lastMove);
				if(setFalta.getCodigo()!=RegChecadaHumanConstante.SUCCES) {
					LOG.info(setFalta.getMsj());
					out.setCodigo(setFalta.getCodigo());
					return out;
				}				
			}
		}
	
		servicio = RegChecadaHumanConstante.HUCATCTGRAL_SERVICE + "cia=" + in.getNumCia();
		HuCatCtGralDto[] list1 = template.getForObject(servicio, HuCatCtGralDto[].class);
		List<HuCatCtGralDto> catCtGral = new ArrayList<HuCatCtGralDto>();
		catCtGral = Arrays.asList(list1);
		
		if(catCtGral.get(0).getAnalisisFaltas().equalsIgnoreCase("S")) {
			Map<String, String> permisosParam = new HashMap<>();
			permisosParam.put("numCia", String.valueOf(in.getNumCia()));
			permisosParam.put("numEmp", String.valueOf(in.getNumEmp()));
			permisosParam.put("fecha", fechaPasada);
			servicio = RegChecadaHumanConstante.PR_RETARDO_SERVICE;
			PrDto[] prList = template.getForObject(servicio, PrDto[].class, permisosParam);
			
			if(prList==null) {
				SetRetardoOutDto retardoOut = serviceRetardo.setRetardo(in.getNumCia(), in.getNumEmp(), in.getHoraChecada(), lastMoveByDate, in.getMrDto(), maxRnSec, catCtGral.get(0));
				if(retardoOut.getCodigo().equalsIgnoreCase(RegChecadaHumanConstante.ERROR_RETARDO_FALTA)) {
					out.setCodigo(RegChecadaHumanConstante.ERROR_RETARDO_FALTA);
					return out;
				}
			}			
		}		
		
		servicio = RegChecadaHumanConstante.RN_SAVE_SERVICE;
		RnDto inputRn = new RnDto();
		RnPKDto inputPK = new RnPKDto();
		
		inputPK.setRnCia(in.getNumCia());
		inputRn.setRnChequeo("N");
		inputPK.setRnNumEmp(in.getNumEmp());
		inputPK.setRnFecha(LocalDate.parse(fechaPasada, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay());
		inputRn.setRnFecModif(in.getHoraChecada());
		inputRn.setRnHorEnt(in.getHoraChecada());
		inputRn.setRnGafete(BigDecimal.ZERO);
		inputRn.setRnUsuario("eTime");
		inputRn.setRnSts("A");
		inputRn.setRnCausaRetardo(" ");
		inputRn.setRnComida(BigDecimal.ZERO);
		inputRn.setRnEstacion(in.getEstacion());
		inputRn.setRnHoraModif(in.getHoraChecada());
		inputRn.setRnHorEnt(in.getHoraChecada());
		inputRn.setRnFecModif(LocalDate.now().atStartOfDay());
		inputRn.setRnHoraModif(LocalDateTime.now());
		inputPK.setRnSecuencia(maxRnSec);
		inputRn.setRnCentro(lastMove.getRnCentro());
		inputRn.setRnArea(lastMove.getRnArea());
		inputRn.setRnZona(lastMove.getRnZona());
		inputRn.setRnLinea(lastMove.getRnLinea());		
		inputRn.setId(inputPK);
		String guardarRn = template.postForObject(servicio, inputRn, String.class);
		
		if(!guardarRn.equalsIgnoreCase("Registro creado")) {
			out.setCodigo(RegChecadaHumanConstante.ERROR_GENERAL);
			return out;
		}
		
		servicio = RegChecadaHumanConstante.HURELOJREGTXT_SAVE_SERVICE;
		HuRelojRegTxtDto huRelojRegTxtIn = new HuRelojRegTxtDto();
		HuRelojRegTxtPKDto huRelojRegTxtPK = new HuRelojRegTxtPKDto();
		
		huRelojRegTxtPK.setNumEmp(in.getNumEmp());
		huRelojRegTxtIn.setUserId("eTime");
		huRelojRegTxtPK.setCia(in.getNumCia());
		huRelojRegTxtIn.setEstacion(in.getEstacion());
		huRelojRegTxtIn.setCveChecada(in.getTipoChecada());
		huRelojRegTxtPK.setHora(in.getHoraChecada());
		huRelojRegTxtIn.setComida(BigDecimal.ZERO);
		huRelojRegTxtIn.setFechaMov(LocalDateTime.now());
		huRelojRegTxtIn.setMotChecada(" ");
		huRelojRegTxtIn.setStatus("B");
		huRelojRegTxtPK.setFecha(LocalDate.parse(fechaPasada, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay());
		huRelojRegTxtIn.setId(huRelojRegTxtPK);
		String guardarhuRelojRegTxt = template.postForObject(servicio, huRelojRegTxtIn, String.class);
		
		if(!guardarhuRelojRegTxt.equalsIgnoreCase("Registro creado")) {
			out.setCodigo(RegChecadaHumanConstante.ERROR_BASE_DATOS);
			return out;
		}
		out.setCodigo(RegChecadaHumanConstante.SUCCES);
		return out;
	}
}