package com.vsm.etime.service;

import java.time.LocalDateTime;
import com.vsm.etime.dto.RnDto;
import com.vsm.etime.dto.SetFaltaOutDto;

public interface SetFalta {
	SetFaltaOutDto setFalta(Long numCia, Long numEmp, LocalDateTime horaChecada, RnDto lastMove);
}