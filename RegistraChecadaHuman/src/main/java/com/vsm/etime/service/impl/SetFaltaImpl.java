package com.vsm.etime.service.impl;

import java.time.LocalDateTime;
import org.springframework.stereotype.Service;
import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.RnDto;
import com.vsm.etime.dto.SetFaltaOutDto;
import com.vsm.etime.service.SetFalta;

@Service("setFaltaService")
public class SetFaltaImpl implements SetFalta {

	@Override
	public SetFaltaOutDto setFalta(Long numCia, Long numEmp, LocalDateTime horaChecada, RnDto lastMove) {
		// TODO Auto-generated method stub
		SetFaltaOutDto out = new SetFaltaOutDto();
		
		if(lastMove==null) {
			out.setCodigo(RegChecadaHumanConstante.ERROR_RN_NO_EXISTE);
			return out;
		}
		
		if(horaChecada.isAfter(lastMove.getId().getRnFecha())) {
			int anioChecada = horaChecada.getYear();
			int anioLastMove = lastMove.getId().getRnFecha().getYear();
			
			out.setCodigo(RegChecadaHumanConstante.SUCCES);
			return out;			
		}else if(horaChecada.isAfter(lastMove.getId().getRnFecha())) {
			out.setCodigo(RegChecadaHumanConstante.SALIDA_MAYOR_CHECADA);
			return out;
		}else {
			out.setCodigo(RegChecadaHumanConstante.SUCCES);
			return out;
		}
	}
}