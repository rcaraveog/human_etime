package com.vsm.etime.service;

import java.time.LocalDateTime;
import com.vsm.etime.dto.HuCatCtGralDto;
import com.vsm.etime.dto.MrDto;
import com.vsm.etime.dto.RnDto;
import com.vsm.etime.dto.SetRetardoOutDto;

public interface SetRetardo {
	SetRetardoOutDto setRetardo(Long numCia, Long numEmp, LocalDateTime horaChecada, RnDto lastMove, MrDto mr, Long maxRnSec, HuCatCtGralDto huCatCtGral);
}