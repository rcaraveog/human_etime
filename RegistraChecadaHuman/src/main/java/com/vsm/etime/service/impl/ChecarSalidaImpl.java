package com.vsm.etime.service.impl;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.ChecadaSalidaInDto;
import com.vsm.etime.dto.ChecadaSalidaOutDto;
import com.vsm.etime.dto.HuCatRolDetDto;
import com.vsm.etime.dto.HuRelojRegTxtDto;
import com.vsm.etime.dto.RhDto;
import com.vsm.etime.dto.RnDto;
import com.vsm.etime.dto.Ta140Dto;
import com.vsm.etime.service.ChecarSalida;
import com.vsm.etime.service.SetFalta;
import com.vsm.etime.service.SetRetardo;

@Service("checarSalidaService")
public class ChecarSalidaImpl implements ChecarSalida {
	private static final Logger LOG = Logger.getLogger(ChecarSalidaImpl.class.getName());
	
	@Autowired
	SetFalta service;
	
	@Autowired
	SetRetardo serviceRetardo;
	
	@Autowired
	RestTemplate template;
	
	@Override
	public ChecadaSalidaOutDto salida(ChecadaSalidaInDto in) {
		// TODO Auto-generated method stub
		ChecadaSalidaOutDto out = new ChecadaSalidaOutDto();
		String fechaPasada = in.getHoraChecada().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		BigDecimal claveTurno = in.getMrDto().getMrCveTurno();
		BigDecimal claveHorario = in.getMrDto().getMrCveHor();
		
		String servicio = RegChecadaHumanConstante.RELOJREGTXT_SERVICE;
		Map<String, String> relojRegTxtParam = new HashMap<>();
		relojRegTxtParam.put("numCia", String.valueOf(in.getNumCia()));
		relojRegTxtParam.put("numEmp", String.valueOf(in.getNumEmp()));
		relojRegTxtParam.put("fecha", in.getHoraChecada().toString().substring(0,10));
		relojRegTxtParam.put("hora", in.getHoraChecada().toString().replace("T", " ").substring(0,19));		
		HuRelojRegTxtDto relojRegTxt = template.getForObject(servicio, HuRelojRegTxtDto.class, relojRegTxtParam);		
				
		if(relojRegTxt!=null) {
			LOG.info("existe registro en HU_RELOJ_REG_TXT parametros: " + relojRegTxtParam.toString());
			out.setCodigo(RegChecadaHumanConstante.ERROR_CHECADA_DUPLICADA);
			return out;
		}
		
		servicio = RegChecadaHumanConstante.RH_SERVICE;
		Map<String, String> rhParam = new HashMap<>();
		rhParam.put("numCia", String.valueOf(in.getNumCia()));
		rhParam.put("numEmp", String.valueOf(in.getNumEmp()));
		rhParam.put("fecha", in.getHoraChecada().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));		
		RhDto[] rhlist = template.getForObject(servicio, RhDto[].class, rhParam);
		
		if(rhlist!=null) {
			List<RhDto> rhs = new ArrayList<RhDto>();
			rhs = Arrays.asList(rhlist);
			claveTurno = rhs.get(0).getRhCveTurno();
			claveHorario = rhs.get(0).getRhCveHor();
		}
		
		if(claveTurno.equals(BigDecimal.ZERO) && claveHorario.longValue()>0) {
			servicio = RegChecadaHumanConstante.HU_CAT_ROL_DET_SERVICE;
			Map<String, String> huCatRolDetParam = new HashMap<>();
			huCatRolDetParam.put("numCia", String.valueOf(in.getNumCia()));
			huCatRolDetParam.put("cveRol", String.valueOf(claveHorario));
			huCatRolDetParam.put("fechaIni", in.getHoraChecada().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));		
			HuCatRolDetDto[] huCatRolDetlist = template.getForObject(servicio, HuCatRolDetDto[].class, huCatRolDetParam);
			
			if(huCatRolDetlist!=null) {
				List<HuCatRolDetDto> huCatRolDets = new ArrayList<HuCatRolDetDto>();
				huCatRolDets = Arrays.asList(huCatRolDetlist);
				claveTurno = new BigDecimal(huCatRolDets.get(0).getId().getClaveTurno());
			}
			
			if(claveTurno.longValue()<=0){
				out.setCodigo(RegChecadaHumanConstante.ERROR_CLAVE_TURNO_NULL);
				LOG.info("No esta definida la clave del turno");
				return out;
			}
			
			servicio = RegChecadaHumanConstante.TA140_SERVICE;
			Map<String, String> ta140Param = new HashMap<>();
			ta140Param.put("numCia", String.valueOf(in.getNumCia()));
			ta140Param.put("claveTurno", String.valueOf(claveTurno));
			Ta140Dto[] ta140List = template.getForObject(servicio, Ta140Dto[].class, ta140Param);
			
			if(ta140List==null) {
				out.setCodigo(RegChecadaHumanConstante.ERROR_TURNO_NULL);
				LOG.info("No se encuentra el turno");
				return out;
			}
		}
		
		Map<String, String> rnLastMoveParam = new HashMap<>();
		rnLastMoveParam.put("numCia", String.valueOf(in.getNumCia()));
		rnLastMoveParam.put("numEmp", String.valueOf(in.getNumEmp()));
		servicio = RegChecadaHumanConstante.RN_LAST_MOVE_SERVICE;		
		RnDto lastMove = template.getForObject(servicio, RnDto.class, rnLastMoveParam);
		
		if(lastMove==null) {
			out.setCodigo(RegChecadaHumanConstante.ERROR_SALIDA);
			LOG.info("No existen checadas");
			return out;
		}	
		
		if(lastMove.getRnHorSal()!=null) {
			out.setCodigo(RegChecadaHumanConstante.ERROR_SALIDA);
			LOG.info("El utimo movimiento es salida");
			return out;
		}
		
		if(lastMove.getRnHorEnt().isAfter(in.getHoraChecada())) {
			out.setCodigo(RegChecadaHumanConstante.ENTRADA_MAYOR_CHECADA);
			LOG.info("La entrada es mayor a la salida: " + lastMove.getRnHorEnt().toString());
			return out;
		}
	
		Map<String, String> rnLastMoveFechaParam = new HashMap<>();
		rnLastMoveFechaParam.put("numCia", String.valueOf(in.getNumCia()));
		rnLastMoveFechaParam.put("numEmp", String.valueOf(in.getNumEmp()));
		rnLastMoveFechaParam.put("date", fechaPasada);
		servicio = RegChecadaHumanConstante.RN_FALTA_SERVICE;		
		RnDto lastMoveByDate = template.getForObject(servicio, RnDto.class, rnLastMoveFechaParam);
		
		if(lastMoveByDate!=null) {
			Map<String, String> rnUpdateParam = new HashMap<>();
			rnUpdateParam.put("numCia", String.valueOf(in.getNumCia()));
			rnUpdateParam.put("numEmp", String.valueOf(in.getNumEmp()));
			rnUpdateParam.put("numSec", String.valueOf(in.getNumEmp()));
			rnUpdateParam.put("date", fechaPasada);
			rnUpdateParam.put("fechaSalida", in.getHoraChecada().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			servicio = RegChecadaHumanConstante.UPDATE_RN_FALTA_SERVICE;			
			String updateResult = template.postForObject(servicio, null, String.class, rnUpdateParam);
			
			if(!updateResult.equalsIgnoreCase("registro modificado")) {
				out.setCodigo(RegChecadaHumanConstante.ERROR_BASE_DATOS);
				return out;
			}
		}
		out.setCodigo(RegChecadaHumanConstante.SUCCES);
		return out;
	}
}