package com.vsm.etime.service;

import com.vsm.etime.dto.ValidaEstacionDto;

public interface ValidaEstacion {
	ValidaEstacionDto valEstacion(String numCia, String numEmp, String prioridad,
										  String latitud, String longitud, String beacon);
}