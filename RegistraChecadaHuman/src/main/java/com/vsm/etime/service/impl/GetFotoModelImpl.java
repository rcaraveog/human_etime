package com.vsm.etime.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.vsm.etime.constant.RegChecadaHumanConstante;
import com.vsm.etime.dto.HuCatToGralDto;
import com.vsm.etime.dto.HuRutasFileServerDto;
import com.vsm.etime.service.GetFotoModel;

@Service("getFotoModelService")
public class GetFotoModelImpl implements GetFotoModel {
	private static final Logger LOG = Logger.getLogger(GetFotoModelImpl.class.getName());
	
	@Autowired
	RestTemplate template;
	
	@Override
	public Map<String,String> GetFoto(long numCia, long numEmp) {
		String servicio = RegChecadaHumanConstante.PARAMETRO_HUMAN_SERVICE;
		Formatter fmtEmp = new Formatter();
		fmtEmp.format("%010d",numEmp);
		String valorAlfanumerico = null;
		String valorRuta = null;
		
		HuCatToGralDto[] respon1 = template.getForObject(servicio, HuCatToGralDto[].class, "PATH_ETIME");
		List<HuCatToGralDto> listGral = new ArrayList<HuCatToGralDto>();
		listGral = Arrays.asList(respon1);
		for (int i = 0; i < listGral.size(); i++) {
			HuCatToGralDto e = listGral.get(i);
			valorAlfanumerico = e.getValorAlfanumerico();
		}
		
		HuRutasFileServerDto[] respon2 = template.getForObject(servicio, HuRutasFileServerDto[].class, "FOTO");
		List<HuRutasFileServerDto> listRutas = new ArrayList<HuRutasFileServerDto>();
		listRutas = Arrays.asList(respon2);
		for (int i = 0; i < listRutas.size(); i++) {
			HuRutasFileServerDto e = listRutas.get(i);
			valorRuta = e.getValorRuta();
		}

		String filePath = valorAlfanumerico + "\\" + numCia + "\\" + valorRuta + "\\" + fmtEmp	+ ".JPG";
		fmtEmp.close();
		filePath = filePath.replace("\\", "/");
		filePath = filePath.replace("/", File.separator);
		LOG.info("Path FotoModel: " + filePath);
		
		File file = new File(filePath);
		String lastModified = String.valueOf(file.lastModified());
		Map<String, String> map = new HashMap<String, String>();
		map.put("fechaFoto", lastModified);
		
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			// Reading a file from file system
			byte fileData[] = new byte[(int) file.length()];
			imageInFile.read(fileData);
			map.put("foto",Base64.getEncoder().encodeToString(fileData));
		} catch (FileNotFoundException e) {
			LOG.info("File not found" + e);
			map.put("foto","File not found" + e);
			return map;
		} catch (IOException ioe) {
			LOG.info("Exception while reading the file " + ioe);
			map.put("foto","Exception while reading the file " + ioe);
			return map;
		}
		
		return map;
	}
	
	@Override
	public String encryptPassword(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] enc = md.digest();
			String md5Sum = Base64.getEncoder().encodeToString(enc);
			return md5Sum;
		} catch (NoSuchAlgorithmException nsae) {
			System.out.println(nsae.getMessage());
			return null;
		}
	}
}