package com.vsm.etime.util;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SBeacons {
	List<String> beacons;
}