package com.vsm.etime.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Informacion {
	String cia;
	String numeroEmpleado;
	String nombre;
	String tiempoBloqueoInicial;
	String intentoBloqueoInicial;
	String tiempoBloqueoReconocimiento;
	String intentoBloqueoReconocimiento;
	String enrolador;
	String usuarioExterno;
	String razonSocial;
	String foto;
	String ultimaChecada;
	String tipoChecada;
	String statusFoto;
	String prioridad;
	String avisoPrivacidad;
}