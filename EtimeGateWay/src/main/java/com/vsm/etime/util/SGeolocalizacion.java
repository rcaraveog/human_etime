package com.vsm.etime.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SGeolocalizacion {
	String latitud;
	String longitud;
}