package com.vsm.etime.util;

import java.util.List;
import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Return {
	String error;
	String resultado;
	Informacion informacion;
	List<Map<String,String>> beacons;
}