package com.vsm.etime.constant;

public interface EtimeSaveConstante {
	public static final String SUCCES = System.getenv().get("GET_COD_ET000");
	public static final String CONSULTA_MOVIMIENTO_SERVICE = System.getenv().get("GET_CONSULTA_MOVIMIENTO");
	public static final String CONSULTA_EMPLEADO_SERVICE = System.getenv().get("GET_CONSULTA_EMPLEADO");
	public static final String CONSULTA_AMBIENTE_SERVICE = System.getenv().get("GET_CONSULTA_AMBIENTE");
	public static final String CONSULTA_AMBIENTE_BY_CYA_SERVICE = System.getenv().get("GET_CONSULTA_AMBIENTE_BY_CYA");
	public static final String REGISTRA_CHECADA_HUMAN_SERVICE = System.getenv().get("REGISTRA_CHECDA_HUMAN");
	public static final String REGISTRA_CHECADA_ET_SERVICE = System.getenv().get("REGISTRA_CHECDA_ET");
	
	//Errores Constantes
	public static final String MENSAJE_ERROR_GENERAL = System.getenv().get("GET_MENSAJE_ERROR_GENERAL");
	public static final String ERROR_GENERAL = System.getenv().get("GET_COD_ERROR_EG000");
	public static final String ERROR_ENTRADA_NULL = System.getenv().get("GET_COD_ERROR_ET006");
	public static final String COMPANIA_NULA = System.getenv().get("GET_COD_ERROR_ET001");
	public static final String EMPLEADO_NULO = System.getenv().get("GET_COD_ERROR_ET002");
	public static final String FECHA_NULO = System.getenv().get("GET_COD_ERROR_ET025");
	public static final String ERROR_TIPO_ENTRADA = System.getenv().get("GET_COD_ERROR_ET026");
	public static final String ERROR_FOTO = System.getenv().get("GET_COD_ERROR_ET027");
	public static final String ERROR_PRIORIDAD = System.getenv().get("GET_COD_ERROR_ET028");
}