package com.vsm.etime.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import com.vsm.etime.constant.EtimeSaveConstante;
import com.vsm.etime.dto.ConsultaAmbByCiaInDto;
import com.vsm.etime.dto.ConsultaAmbByCiaOutDto;
import com.vsm.etime.dto.ConsultaAmbInDto;
import com.vsm.etime.dto.ConsultaAmbOutDto;
import com.vsm.etime.dto.ConsultaEmpleadoInDto;
import com.vsm.etime.dto.ConsultaEmpleadoOutDto;
import com.vsm.etime.dto.ConsultaMovimientoInDto;
import com.vsm.etime.dto.ConsultaMovimientoOutDto;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/HumaneTime")
public class EtimeListsController {
	@Autowired
	RestTemplate template;

	private String urlCM=EtimeSaveConstante.CONSULTA_MOVIMIENTO_SERVICE;
    private String urlCE=EtimeSaveConstante.CONSULTA_EMPLEADO_SERVICE;
    private String urlCA=EtimeSaveConstante.CONSULTA_AMBIENTE_SERVICE;
    private String urlCAc=EtimeSaveConstante.CONSULTA_AMBIENTE_BY_CYA_SERVICE;
    private String codigoError=EtimeSaveConstante.ERROR_GENERAL;
    private String mensajeError=EtimeSaveConstante.MENSAJE_ERROR_GENERAL;
	
	@GetMapping(value="/consultaMovimento", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaMovimientoOutDto> consultaMovimento (@RequestBody ConsultaMovimientoInDto in){
		Map<String, String> vars = new HashMap<>();
		vars.put("numCia", in.getNumeroCompania()==null||in.getNumeroCompania().isEmpty() ? "0" : in.getNumeroCompania());
		vars.put("numEmp", in.getEmpleado()==null ||in.getEmpleado().isEmpty() ? "0" : in.getEmpleado());
		vars.put("prioridad", in.getConsultaPrioridad()==null ||in.getConsultaPrioridad().isEmpty() ? "flase" : in.getConsultaPrioridad());
		
		try {
			ConsultaMovimientoOutDto out=template.getForObject(urlCM, ConsultaMovimientoOutDto.class, vars);
			return new ResponseEntity<ConsultaMovimientoOutDto>(out,HttpStatus.OK);
		}
		catch(HttpStatusCodeException exception) {
			HttpHeaders headers=new HttpHeaders();
			headers.add("error", mensajeError);
			headers.add("errorDesc", exception.getResponseBodyAsString());
			ConsultaMovimientoOutDto out = new ConsultaMovimientoOutDto();
			out.setCodigo(codigoError);
			return new ResponseEntity<ConsultaMovimientoOutDto>(out,headers,exception.getStatusCode());
		}		
	}
	
	@GetMapping(value="/consultaEmpleado", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaEmpleadoOutDto> consultaEmpleado (@RequestBody ConsultaEmpleadoInDto in){
		Map<String, String> vars = new HashMap<>();
		vars.put("numCia", in.getCompania()==null||in.getCompania().isEmpty() ? "0" : in.getCompania());
		vars.put("numEmp", in.getEmpleado()==null ||in.getEmpleado().isEmpty() ? "0" : in.getEmpleado());
		vars.put("fechaFoto", in.getFechaFoto()==null ||in.getFechaFoto().isEmpty() ? " " : in.getFechaFoto());
		
		try {
			ConsultaEmpleadoOutDto out=template.getForObject(urlCE, ConsultaEmpleadoOutDto.class, vars);
			return new ResponseEntity<ConsultaEmpleadoOutDto>(out,HttpStatus.OK);
		}
		catch(HttpStatusCodeException exception) {
			HttpHeaders headers=new HttpHeaders();
			headers.add("error", mensajeError);
			headers.add("errorDesc", exception.getResponseBodyAsString());
			ConsultaEmpleadoOutDto out = new ConsultaEmpleadoOutDto();
			out.setCodigo(codigoError);
			return new ResponseEntity<ConsultaEmpleadoOutDto>(out,headers,exception.getStatusCode());
		}		
	}
	
	@GetMapping(value="/consultaAmbiente", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaAmbOutDto> consultaAmbiente (@RequestBody ConsultaAmbInDto in){
		Map<String, String> vars = new HashMap<>();
		vars.put("email", in.getEmail()==null||in.getEmail().isEmpty() ? " " : in.getEmail());
				
		try {
			ConsultaAmbOutDto out=template.getForObject(urlCA, ConsultaAmbOutDto.class, vars);
			return new ResponseEntity<ConsultaAmbOutDto>(out,HttpStatus.OK);
		}
		catch(HttpStatusCodeException exception) {
			HttpHeaders headers=new HttpHeaders();
			headers.add("error", mensajeError);
			headers.add("errorDesc", exception.getResponseBodyAsString());
			ConsultaAmbOutDto out = new ConsultaAmbOutDto();
			out.setCodigo(codigoError);
			return new ResponseEntity<ConsultaAmbOutDto>(out,headers,exception.getStatusCode());
		}		
	}
	
	@GetMapping(value="/consultaAmbienteByCia", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConsultaAmbByCiaOutDto> consultaAmbienteByCia (@RequestBody ConsultaAmbByCiaInDto in){
		Map<String, String> vars = new HashMap<>();
		vars.put("ciaCve", in.getClaveCompania()==null||in.getClaveCompania().trim().isEmpty() ? " " : in.getClaveCompania());
		vars.put("ciaNum", in.getNumeroCompania()==null||in.getNumeroCompania().trim().isEmpty() ? "0" : in.getNumeroCompania());
		vars.put("empNum", in.getNumeroEmpleado()==null||in.getNumeroEmpleado().trim().isEmpty() ? "0" : in.getNumeroEmpleado());				
		try {
			ConsultaAmbByCiaOutDto out=template.getForObject(urlCAc, ConsultaAmbByCiaOutDto.class, vars);
			return new ResponseEntity<ConsultaAmbByCiaOutDto>(out,HttpStatus.OK);
		}
		catch(HttpStatusCodeException exception) {
			HttpHeaders headers=new HttpHeaders();
			headers.add("error", mensajeError);
			headers.add("errorDesc", exception.getResponseBodyAsString());
			ConsultaAmbByCiaOutDto out = new ConsultaAmbByCiaOutDto();
			out.setCodigo(codigoError);
			return new ResponseEntity<ConsultaAmbByCiaOutDto>(out,headers,exception.getStatusCode());
		}		
	}
}