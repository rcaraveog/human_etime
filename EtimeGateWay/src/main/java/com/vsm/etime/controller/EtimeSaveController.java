package com.vsm.etime.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.vsm.etime.constant.EtimeSaveConstante;
import com.vsm.etime.dto.ConsultaAmbByCiaOutDto;
import com.vsm.etime.dto.RegistraChecadaEtInDto;
import com.vsm.etime.dto.RegistraChecadaEtOutDto;
import com.vsm.etime.dto.RegistraChecadaInDto;
import com.vsm.etime.dto.RegistraChedadaOutDto;
import com.vsm.etime.dto.RequestHumaneTimeDto;
import com.vsm.etime.dto.ResponseHumaneTimeDto;
import com.vsm.etime.dto.RootOutDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/HumaneTime")
@Api(tags = "HumaneTime Api GateWay")
public class EtimeSaveController {
	@Autowired
	RestTemplate template;
	
	@ApiOperation(value = "Registrar Checada de Human eLand Unificado")
	@PostMapping(value="/registraChecada/unificado", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RegistraChedadaOutDto> regChqDesvinculado (@RequestBody RegistraChecadaInDto in){
		String servicio = EtimeSaveConstante.CONSULTA_AMBIENTE_BY_CYA_SERVICE;
		RegistraChedadaOutDto out = new RegistraChedadaOutDto();
		RootOutDto rootOut = new RootOutDto();		
		
		if (in == null) {
			rootOut.setCodigo(EtimeSaveConstante.ERROR_ENTRADA_NULL);			
		}
		
		if (Integer.parseInt(in.getRoot().getNumeroCompania()) <= 0) {
			rootOut.setCodigo(EtimeSaveConstante.COMPANIA_NULA);			
		} else if (Integer.parseInt(in.getRoot().getEmpleado()) <= 0) {
			rootOut.setCodigo(EtimeSaveConstante.EMPLEADO_NULO);
		} else if (in.getRoot().getFechaHoraChecada() == null) {
			rootOut.setCodigo(EtimeSaveConstante.FECHA_NULO);
		} else if (!("E".equals(in.getRoot().getTipoChecada()) || "S".equals(in.getRoot().getTipoChecada()))) {
			rootOut.setCodigo(EtimeSaveConstante.ERROR_TIPO_ENTRADA);
		} else if (in.getRoot().getFoto() == null || !in.getRoot().getFoto().contains("/9j/4AAQSkZ")) {
			rootOut.setCodigo(EtimeSaveConstante.ERROR_FOTO);
		} else if (Integer.parseInt(in.getRoot().getPrioridad()) <= 0 || Integer.parseInt(in.getRoot().getPrioridad()) >= 5) {
			rootOut.setCodigo(EtimeSaveConstante.ERROR_PRIORIDAD);
		}
		
		if(rootOut.getCodigo()!=null) {
			out.setRoot(rootOut);
			return new ResponseEntity<RegistraChedadaOutDto>(out,HttpStatus.OK);
		}
		
		Map<String, String> vars = new HashMap<>();
		vars.put("ciaCve", " ");
		vars.put("ciaNum", in.getRoot().getNumeroCompania());
		vars.put("empNum", in.getRoot().getEmpleado());				
		try {
			ConsultaAmbByCiaOutDto outCia=template.getForObject(servicio, ConsultaAmbByCiaOutDto.class, vars);
			
			if(outCia.getCodigo().equalsIgnoreCase(EtimeSaveConstante.SUCCES)) {
				if(outCia.getAmbiente().equalsIgnoreCase("HU")) {
					servicio = EtimeSaveConstante.REGISTRA_CHECADA_HUMAN_SERVICE;
					RequestHumaneTimeDto inHuman = new RequestHumaneTimeDto();
					inHuman.setBeacon(in.getRoot().getsBeacons().getBeacons().toArray(new String[0]));
					inHuman.setFechaChecada(in.getRoot().getFechaHoraChecada());
					inHuman.setFoto(in.getRoot().getFoto());
					inHuman.getGeolocalizacion().setLatitud(Long.parseLong(in.getRoot().getsGeolocalizacion().getLatitud()));
					inHuman.getGeolocalizacion().setLongitud(Long.parseLong(in.getRoot().getsGeolocalizacion().getLongitud()));
					inHuman.setNumCia(Integer.parseInt(in.getRoot().getNumeroCompania()));
					inHuman.setNumEmp(Integer.parseInt(in.getRoot().getEmpleado()));
					inHuman.setPrioridad(Integer.parseInt(in.getRoot().getPrioridad()));
					inHuman.setTipoChecada(in.getRoot().getPrioridad());					
					ResponseHumaneTimeDto outHuman = template.postForObject(servicio, inHuman, ResponseHumaneTimeDto.class);
					
					rootOut.setCodigo(outHuman.getCodigo());
					rootOut.setEstacion(outHuman.getEstacion()==null?" ":outHuman.getEstacion());
				}else {
					servicio = EtimeSaveConstante.REGISTRA_CHECADA_ET_SERVICE;
					RegistraChecadaEtInDto inEt = new RegistraChecadaEtInDto();
					Map<String, String> beacons = new HashMap<>();
					vars.put("beacon", in.getRoot().getsBeacons().getBeacons().get(0));
					inEt.setEmpleado(in.getRoot().getEmpleado());
					inEt.setFechaHoraChecada(in.getRoot().getFechaHoraChecada().toString());
					inEt.setFoto(in.getRoot().getFoto());
					inEt.setNumeroCompania(in.getRoot().getNumeroCompania());
					inEt.setPrioridad(in.getRoot().getPrioridad());
					inEt.getsBeacons().getBeacons().set(0, beacons);
					inEt.getsGeolocalizacion().setLatitud(in.getRoot().getsGeolocalizacion().getLatitud());
					inEt.getsGeolocalizacion().setLongitud(in.getRoot().getsGeolocalizacion().getLongitud());
					inEt.setTipoChecada(in.getRoot().getTipoChecada());
					RegistraChecadaEtOutDto outHuman = template.postForObject(servicio, inEt, RegistraChecadaEtOutDto.class);
					
					rootOut.setCodigo(outHuman.getCodigo());
					rootOut.setEstacion(outHuman.getEstacion()==null?" ":outHuman.getEstacion());
				}				
			}else {
				rootOut.setCodigo(outCia.getCodigo());
			}
			out.setRoot(rootOut);
			return new ResponseEntity<RegistraChedadaOutDto>(out,HttpStatus.OK);
		}
		catch(HttpStatusCodeException exception) {
			HttpHeaders headers=new HttpHeaders();
			headers.add("error", EtimeSaveConstante.MENSAJE_ERROR_GENERAL);
			headers.add("errorDesc", exception.getResponseBodyAsString());
			rootOut.setCodigo(EtimeSaveConstante.ERROR_GENERAL);
			out.setRoot(rootOut);
			return new ResponseEntity<RegistraChedadaOutDto>(out,headers,exception.getStatusCode());
		}		
	}
}