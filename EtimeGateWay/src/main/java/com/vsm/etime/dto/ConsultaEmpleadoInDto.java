package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaEmpleadoInDto {
	String compania;
	String empleado;
	String fechaFoto;
}