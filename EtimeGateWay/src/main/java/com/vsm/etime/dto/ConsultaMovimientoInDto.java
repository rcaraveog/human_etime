package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaMovimientoInDto {
	String numeroCompania;
	String empleado;
	String consultaPrioridad;
}