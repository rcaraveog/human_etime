package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaMovimientoOutDto {
	String codigo;
	String prioridad;
	String horaChecada;
	String tipoChecada;
}