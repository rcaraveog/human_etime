package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RegistraChecadaEtOutDto {
	boolean isError;
	String  errorMessage;
	
	String codigo;
	String estacion;
}