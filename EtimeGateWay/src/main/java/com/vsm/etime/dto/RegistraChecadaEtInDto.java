package com.vsm.etime.dto;

public class RegistraChecadaEtInDto {
	String numeroCompania;
	String empleado;
	String fechaHoraChecada;
	String tipoChecada;
	String foto;
	String prioridad;
	
	SGeolocalizacion sGeolocalizacion;
	SBeacons sBeacons;
	
	public String getNumeroCompania() {
		return numeroCompania;
	}
	public void setNumeroCompania(String numeroCompania) {
		this.numeroCompania = numeroCompania;
	}
	public String getEmpleado() {
		return empleado;
	}
	public void setEmpleado(String empleado) {
		this.empleado = empleado;
	}
	public String getFechaHoraChecada() {
		return fechaHoraChecada;
	}
	public void setFechaHoraChecada(String fechaHoraChecada) {
		this.fechaHoraChecada = fechaHoraChecada;
	}
	public String getTipoChecada() {
		return tipoChecada;
	}
	public void setTipoChecada(String tipoChecada) {
		this.tipoChecada = tipoChecada;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	public SGeolocalizacion getsGeolocalizacion() {
		return sGeolocalizacion;
	}
	public void setsGeolocalizacion(SGeolocalizacion sGeolocalizacion) {
		this.sGeolocalizacion = sGeolocalizacion;
	}
	public SBeacons getsBeacons() {
		return sBeacons;
	}
	public void setsBeacons(SBeacons sBeacons) {
		this.sBeacons = sBeacons;
	}
}