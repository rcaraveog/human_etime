package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaAmbByCiaOutDto {
	String codigo;
	String ambiente;
	String compania;
	String numEmpleado;	
}