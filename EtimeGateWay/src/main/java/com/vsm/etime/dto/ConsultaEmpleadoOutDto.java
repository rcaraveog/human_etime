package com.vsm.etime.dto;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaEmpleadoOutDto {	
	String codigo;
	String nombre;
	String numeroCompania;	
	String foto;
	String fechaFoto;	
	List<Map<String,String>> beacons;
}