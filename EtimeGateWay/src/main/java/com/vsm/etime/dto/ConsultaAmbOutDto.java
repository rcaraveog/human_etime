package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaAmbOutDto {
	String codigo;
	String ambiente;
	String compania;	
	String numCia;
	String numEmpleado;	
}