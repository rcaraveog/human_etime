package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaAmbByCiaInDto {
	String claveCompania;
	String numeroCompania;
	String numeroEmpleado;
}