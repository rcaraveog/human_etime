package com.vsm.etime.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseHumaneTimeDto {
	private String codigo;
	private String estacion;
}