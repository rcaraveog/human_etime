package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtCiaDto{
	private long idCia;
	private String cve;
	private String cveIdioma;
	private String estatus;
	private String nombre;
}