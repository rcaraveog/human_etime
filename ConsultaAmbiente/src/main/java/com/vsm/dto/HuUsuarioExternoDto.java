package com.vsm.dto;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HuUsuarioExternoDto {
	private long userSkey;
	private String apellMat;
	private String apellPat;
	private String claveId;
	private BigDecimal domClavePaisTel;
	private BigDecimal domCodAreaTel;
	private BigDecimal domTel;
	private String domicilio;
	private String email;
	private Object fechaMov;
	private BigDecimal nip;
	private String nombre;
	private BigDecimal ofClavePaisTel;
	private BigDecimal ofCodAreaTel;
	private BigDecimal ofExtTel;
	private BigDecimal ofTel;
	private String status;
	private String supervisor;
	private String userId;
}