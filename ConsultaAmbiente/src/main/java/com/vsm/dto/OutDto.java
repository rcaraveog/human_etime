package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OutDto {
	boolean isError;
	String  errorMessage;
	String  nombreEmpleado;
	
	String codigo;
	String ambiente;
	String compania;	
	String numCia;
	String numEmpleado;	
}