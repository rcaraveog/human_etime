package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OutByCiaDto {
	boolean isError;
	String  errorMessage;
	
	String codigo;
	String ambiente;
	String compania;
	String numEmpleado;	
}