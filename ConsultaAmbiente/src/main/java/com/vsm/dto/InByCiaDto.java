package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InByCiaDto {
	String claveCompania;
	String numeroCompania;
	String numeroEmpleado;
}