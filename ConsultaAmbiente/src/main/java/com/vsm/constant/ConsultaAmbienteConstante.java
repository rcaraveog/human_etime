package com.vsm.constant;

public interface ConsultaAmbienteConstante {
	public static final String SERVICIO_DB_HUMAN = System.getenv().get("GET_SELECT_DB");
	public static final String SERVICIO_DB_ETIME = System.getenv().get("GET_SELECT_DB_ETIME");
	
	//Errores Constantes
	public static final String ERROR_GENERAL = System.getenv().get("GET_COD_ERROR_EG000");
	public static final String DESCRIPCION_ERROR_GENERAL = System.getenv().get("GET_DESC_ERROR_EG000");
	public static final String SUCCES = System.getenv().get("GET_COD_ERROR_ET000");
	public static final String EMAIL_NULO = System.getenv().get("GET_COD_ERROR_ET201");
	public static final String COMPANIA_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET214");
	public static final String EMPLEADO_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET222");
	public static final String EMPLEADO_ET_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET320");
	public static final String COMPANIA_NULO = System.getenv().get("GET_COD_ERROR_ET262");
	public static final String EMPLEADO_NULO = System.getenv().get("GET_COD_ERROR_ET260");
}