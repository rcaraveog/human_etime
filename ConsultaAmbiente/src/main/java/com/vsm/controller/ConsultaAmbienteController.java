package com.vsm.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.vsm.constant.ConsultaAmbienteConstante;
import com.vsm.dto.EtCiaDto;
import com.vsm.dto.EtUsuarioDto;
import com.vsm.dto.HuCompaniaDto;
import com.vsm.dto.HuEmplDto;
import com.vsm.dto.HuUsuarioExternoDto;
import com.vsm.dto.OutDto;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/consultaAmbiente")
public class ConsultaAmbienteController {
	@Autowired
	RestTemplate template;
	
    private String url1=ConsultaAmbienteConstante.SERVICIO_DB_HUMAN;
    private String url2=ConsultaAmbienteConstante.SERVICIO_DB_ETIME;
    private String errorMail=ConsultaAmbienteConstante.EMAIL_NULO;
	private String errorCia=ConsultaAmbienteConstante.COMPANIA_NULO;
	private String errorEmp=ConsultaAmbienteConstante.EMPLEADO_NULO;
	private String ciaND=ConsultaAmbienteConstante.COMPANIA_NO_EXISTE;
	private String empND=ConsultaAmbienteConstante.EMPLEADO_NO_EXISTE;
	private String empEtND=ConsultaAmbienteConstante.EMPLEADO_ET_NO_EXISTE;
    private String succes=ConsultaAmbienteConstante.SUCCES;
	
	@GetMapping(value="/getAmbinte/{email}", produces=MediaType.APPLICATION_JSON_VALUE)
	public OutDto getAmbinte (@PathVariable("email") String email){
		String servicio = "DataBase/HuEmpls/getEmpleadoByMail/{email}";
		OutDto out = new OutDto();
		
		if(email == null || email.trim().isEmpty()) {			
			out.setCodigo(errorMail);
			return out;
		}
		
		Map<String, String> emailParam = new HashMap<>();
		emailParam.put("email", email);
		
		HuEmplDto emp = template.getForObject(url1+servicio, HuEmplDto.class, emailParam);
		if (emp!=null) {
			Map<String, Long> numCiaParam = new HashMap<>();
			numCiaParam.put("numCia", emp.getId().getNumCia());
			
			servicio = "DataBase/HuCompania/getCiaByNumber/{numCia}";
			HuCompaniaDto cia = template.getForObject(url1+servicio, HuCompaniaDto.class, numCiaParam);
			if(cia!=null) {
				out.setCodigo(succes);
				out.setAmbiente("HU");
				out.setCompania(cia.getClaveCompania());
				out.setNumCia(String.valueOf(emp.getId().getNumCia()));
				out.setNumEmpleado(String.valueOf(emp.getId().getNumEmp()));
			}else {
				out.setCodigo(ciaND);
				return out;
			}
		}else {
			servicio = "DataBase/HuUsrExt/getEmpleadoByMail/{email}";
			HuUsuarioExternoDto empExt = template.getForObject(url1+servicio, HuUsuarioExternoDto.class, emailParam);
			if (empExt!=null) {
				String numEmp = String.valueOf(empExt.getUserSkey()).substring(String.valueOf(empExt.getUserSkey()).length()-9);
				String numCia = String.valueOf(empExt.getUserSkey()).substring(0, String.valueOf(empExt.getUserSkey()).length()-9);
				Map<String, Long> numCiaParam = new HashMap<>();
				numCiaParam.put("numCia", Long.parseLong(numCia));
				
				servicio = "DataBase/HuCompania/getCiaByNumber/{numCia}";
				HuCompaniaDto cia = template.getForObject(url1+servicio, HuCompaniaDto.class, numCiaParam);
				if(cia!=null) {
					out.setCodigo(succes);
					out.setAmbiente("HU");
					out.setCompania(cia.getClaveCompania());
					out.setNumCia(numCia);
					out.setNumEmpleado(numEmp);
				}else {
					out.setCodigo(ciaND);
					return out;
				}
			}else {
				servicio = "DataBase/EtUsuario/getEmpleadoByMail/{email}";
				EtUsuarioDto empEt = template.getForObject(url2+servicio, EtUsuarioDto.class, emailParam);
				if (empEt!=null) {
					out.setCodigo(succes);
					out.setAmbiente("ET");
				}else {
					out.setCodigo(empND);
					return out;
				}
			}
		}
		
		return out;
	}
	
	@GetMapping(value="/getAmbinteByCia/{ciaCve}/{ciaNum}/{empNum}", produces=MediaType.APPLICATION_JSON_VALUE)
	public OutDto getAmbinteByCia (@PathVariable("ciaCve") String ciaCve, @PathVariable("ciaNum") String ciaNum, @PathVariable("empNum") String empNum){
		String servicio = "DataBase/HuEmpls/getEmpleadoByMail/{email}";
		OutDto out = new OutDto();
		HuCompaniaDto cia = new HuCompaniaDto();
		EtCiaDto ciaEt = new EtCiaDto();
		
		if(empNum == null || empNum.trim().isEmpty() || Integer.parseInt(empNum) <= 0) {			
			out.setCodigo(errorEmp);
			return out;
		}
		
		if((ciaNum == null || ciaNum.trim().isEmpty() || Integer.parseInt(ciaNum) <= 0)&&
		   (ciaCve == null || ciaCve.trim().isEmpty())) {			
			out.setCodigo(errorCia);
			return out;
		}
		
		if(ciaNum!=null && Integer.parseInt(ciaNum)>0) {
			Map<String, Long> numCiaParam = new HashMap<>();
			numCiaParam.put("numCia", Long.parseLong(ciaNum));
			servicio = "DataBase/HuCompania/getCiaByNumber/{numCia}";
			cia = template.getForObject(url1+servicio, HuCompaniaDto.class, numCiaParam);
		}
		
		if((ciaNum==null || Integer.parseInt(ciaNum)==0) && (ciaCve!=null&&!ciaCve.trim().isEmpty())) {
			Map<String, String> getCiaParam = new HashMap<>();
			getCiaParam.put("ciaNom", ciaCve);
			servicio = "DataBase/HuCompania/getCiaByname/{ciaNom}";
			cia = template.getForObject(url1+servicio, HuCompaniaDto.class, getCiaParam);
		}
		
		if(cia!=null) {
			servicio = "DataBase/HuEmpls/getEmpleado/{numCia}/{numEmp}";
			Map<String, Long> getEmpParam = new HashMap<>();
			getEmpParam.put("numCia", cia.getNumeroCompania());
			getEmpParam.put("numEmp", Long.parseLong(empNum));
			
			HuEmplDto emp = template.getForObject(url1+servicio, HuEmplDto.class, getEmpParam);
			if(emp != null) {
				out.setCodigo(succes);
				out.setAmbiente("HU");
				out.setCompania(cia.getClaveCompania());
				out.setNumEmpleado(String.valueOf(empNum));
				out.setNumCia(String.valueOf(cia.getNumeroCompania()));
				out.setNombreEmpleado(emp.getNombre()+ " " + emp.getApellPat() + " " + emp.getApellMat());
			}else {
				out.setCodigo(empEtND);
				return out;
			}
		}else {
			if(ciaNum!=null && Integer.parseInt(ciaNum)>0) {
				Map<String, Long> numCiaParam = new HashMap<>();
				numCiaParam.put("idCia", Long.parseLong(ciaNum));
				servicio = "DataBase/EtCia/getCia/{idCia}";
				ciaEt = template.getForObject(url2+servicio, EtCiaDto.class, numCiaParam);
			}
			
			if((ciaNum==null || Integer.parseInt(ciaNum)==0) && (ciaCve!=null&&!ciaCve.trim().isEmpty())) {
				Map<String, String> getCiaParam = new HashMap<>();
				getCiaParam.put("cveCia", ciaCve);
				servicio = "DataBase/EtCia/getCiaByCve/{cveCia}";
				ciaEt = template.getForObject(url2+servicio, EtCiaDto.class, getCiaParam);
			}	
			
			if(ciaEt!=null) {				
				servicio = "DataBase/EtEmpleado/getEmpleado/{ciaNum}/{empNum}";
				Map<String, Long> getEmpParam = new HashMap<>();
				getEmpParam.put("ciaNum", ciaEt.getIdCia());
				getEmpParam.put("empNum", Long.parseLong(empNum));
				EtUsuarioDto empEt = template.getForObject(url2+servicio, EtUsuarioDto.class, getEmpParam);
				if(empEt != null) {
					out.setCodigo(succes);
					out.setAmbiente("ET");
					out.setCompania(ciaEt.getCve());
					out.setNumEmpleado(empNum);
					out.setNumCia(String.valueOf(ciaEt.getIdCia()));
					out.setNombreEmpleado(empEt.getNombre()+ " " + empEt.getApPaterno() + " " + empEt.getApMaterno());
				}else {
					out.setCodigo(empEtND);
					return out;
				}				
			}else {
				out.setCodigo(ciaND);
				return out;
			}
		}	
		return out;
	}
}