package com.vsm.etime.dto;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OutDto {
	String codigo;
	HuCompaniaDto huCompaniaDto;
	List<HuClienteConnectDto> huClienteConnectDto;
}