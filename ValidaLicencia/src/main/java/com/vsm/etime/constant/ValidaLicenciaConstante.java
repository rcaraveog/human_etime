package com.vsm.etime.constant;

public interface ValidaLicenciaConstante {
	public static final String SUCCES = System.getenv().get("GET_COD_ET000");
	public static final String COMPANIA_HU_SERVICE = System.getenv().get("GET_COMPANIA_HU");
	public static final String CTE_CONECT_HU_SERVICE = System.getenv().get("GET_CTE_CONECT_HU");
	
	//Errores Constantes
	public static final String ERROR_GENERAL = System.getenv().get("GET_COD_ERROR_EG000");
	public static final String ERROR_LICENCIA = System.getenv().get("GET_COD_ERROR_ET007");
}