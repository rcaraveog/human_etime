package com.vsm.etime.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.vsm.etime.constant.ValidaLicenciaConstante;
import com.vsm.etime.dto.HuClienteConnectDto;
import com.vsm.etime.dto.HuCompaniaDto;
import com.vsm.etime.dto.OutDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/ValidaLicencia")
@Api(tags = "ValidaLicenciaHuman")
public class ValidaLicenciaController {
	private static final Logger LOG = Logger.getLogger(ValidaLicenciaController.class.getName());
		
	@Autowired
	RestTemplate template;
	
	@ApiOperation(value = "Validar licencias de Human eLand Vinculado")
	@GetMapping(value="/validarByCia/{tipoLic}/{ciaNum}", produces=MediaType.APPLICATION_JSON_VALUE)
	public OutDto validarByCia(@PathVariable("tipoLic") String tipoLic, @PathVariable("ciaNum") String ciaNum) {
		OutDto out    = new OutDto();
		String codigo = ValidaLicenciaConstante.SUCCES;
		Map<String, Long> numCiaParam = new HashMap<>();		
		numCiaParam.put("numCia", Long.parseLong(ciaNum));
		String servicio = ValidaLicenciaConstante.COMPANIA_HU_SERVICE;
				
		HuCompaniaDto cia = template.getForObject(servicio, HuCompaniaDto.class, numCiaParam);			
		if(cia == null) {
			LOG.info("Sin coincidencia en HU_COMPANIA parametros: " + numCiaParam.toString());
			codigo = ValidaLicenciaConstante.ERROR_LICENCIA;
			out.setCodigo(codigo);
			return out;
		}
		
		Map<String, String> cteConectParam = new HashMap<>();		
		cteConectParam.put("sigla", cia.getSiglasCliente()==null?" ":cia.getSiglasCliente());
		cteConectParam.put("pais", cia.getHuCatCoGral().getPais()==null?" ":cia.getHuCatCoGral().getPais());
		cteConectParam.put("tipoConexion", tipoLic);
		servicio = ValidaLicenciaConstante.CTE_CONECT_HU_SERVICE;
				
		HuClienteConnectDto[] responceList = template.getForObject(servicio, HuClienteConnectDto[].class, cteConectParam);
		if(responceList==null) {
			LOG.info("Sin coincidencia en HU_CLIENTE_CONNECT  parametros: " + cteConectParam.toString());
			codigo = ValidaLicenciaConstante.ERROR_LICENCIA;
			out.setCodigo(codigo);
			return out;
		}
		
		List<HuClienteConnectDto> cteOut    = new ArrayList<HuClienteConnectDto>();
		List<HuClienteConnectDto> cteConect = Arrays.asList(responceList);
		for(HuClienteConnectDto x:cteConect) {
			if(x.getId().getFechaIni().compareTo(LocalDateTime.now()) <= 0) {
				if(x.getId().getVigencia().equalsIgnoreCase("P")) {
					cteOut.add(x);
				}else if(x.getFechaFin().compareTo(LocalDateTime.now()) >= 0) {
					cteOut.add(x);
				}
			}
		}
		
		if(cteOut.size()<=0) {
			codigo = ValidaLicenciaConstante.ERROR_LICENCIA;
			out.setCodigo(codigo);
			return out;
		}
		
		out.setCodigo(codigo);
		out.setHuCompaniaDto(cia);
		out.setHuClienteConnectDto(cteConect);
		return out;
	}	
}