package com.vsm.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.vsm.constant.ConsultaMovimientoConstante;
import com.vsm.dto.ConsultaAmbByCiaOutDto;
import com.vsm.dto.EtChecadaDto;
import com.vsm.dto.EtEmpEstDto;
import com.vsm.dto.ListEtEmpEstDto;
import com.vsm.dto.OutDto;
import com.vsm.dto.RnDto;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/consultaMovimiento")
public class ConsultaMovimientoController {
	@Autowired
	RestTemplate template;
	
    private String url1=ConsultaMovimientoConstante.SERVICIO_DB_HUMAN;
    private String url2=ConsultaMovimientoConstante.SERVICIO_DB_ETIME;
    private String codigoError=ConsultaMovimientoConstante.ERROR_GENERAL;
	private String errorCia=ConsultaMovimientoConstante.COMPANIA_NULA;
	private String errorEmp=ConsultaMovimientoConstante.EMPLEADO_NULO;
	private String urlCAc=ConsultaMovimientoConstante.CONSULTA_AMBIENTE_SERVICE;
	
	@GetMapping(value="/getMov/{numCia}/{numEmp}/{prioridad}", produces=MediaType.APPLICATION_JSON_VALUE)
	public OutDto getMov (@PathVariable("numCia") String numCia, @PathVariable("numEmp") String numEmp, @PathVariable("prioridad") String prioridad){
		OutDto out = new OutDto();
		Map<String, String> vars = new HashMap<>();
		vars.put("ciaCve", " ");
		vars.put("ciaNum", numCia);
		vars.put("empNum", numEmp);				

		
		if(numCia == null || numCia.isEmpty() || Integer.parseInt(numCia) <= 0) {			
			out.setCodigo(errorCia);
			return out;
		}
		
		if(numEmp == null || numEmp.isEmpty() || Integer.parseInt(numEmp) <= 0) {			
			out.setCodigo(errorEmp);
			return out;
		}
		
		ConsultaAmbByCiaOutDto outCA=template.getForObject(urlCAc, ConsultaAmbByCiaOutDto.class, vars);
		
		if(outCA!=null) {
			if(outCA.getCodigo().equals("ET000") && outCA.getAmbiente().equals("HU")) {
				out = this.getLastMoveHU(numCia, numEmp, prioridad);
				return out;
			}else if(outCA.getCodigo().equals("ET000") && outCA.getAmbiente().equals("ET")) {
				out = this.getLastMoveET(numCia, numEmp, prioridad);
				return out;
			}else {
				out.setCodigo(outCA.getCodigo());
			}
		}
		return out;
	}
	
	private OutDto getLastMoveHU (String numCia, String numEmp, String prioridad) {
		String servicio = "DataBase/getLastMoveCM/{numCia}/{numEmp}";
		OutDto out = new OutDto();
		
		Map<String, Long> vars = new HashMap<>();
		vars.put("numCia", Long.parseLong(numCia));
		vars.put("numEmp", Long.parseLong(numEmp));
		
		RnDto lastMove = template.getForObject(url1+servicio, RnDto.class, vars);
		if(lastMove != null){		
			out.setError(false);
			out.setCodigo("ET000");
			if(lastMove.getRnHorSal()==null) {
				out.setHoraChecada(lastMove.getRnHorEnt().toString());
				out.setTipoChecada("E");
			}else {
				out.setHoraChecada(lastMove.getRnHorSal().toString());
				out.setTipoChecada("S");
			}
		}else {
			out.setCodigo("ET000");
		}
			
		if(prioridad!=null) {
			if(prioridad.equalsIgnoreCase("true")) {	
				servicio = "generic/consultPrioridadCM/{numCia}/{numEmp}";
				ResponseEntity<String[]> responseEntity = template.getForEntity(url1+servicio, String[].class, vars);
				List<String> responseList = Arrays.asList(responseEntity.getBody());
				boolean B = false;
				boolean G = false;
				boolean P = false;
				
				for(String x : responseList) {
					if (x.equalsIgnoreCase("B"))B=true;
					if (x.equalsIgnoreCase("G"))G=true;
					if (x.equalsIgnoreCase("P"))P=true;
				}
				
				if(!B&&!G&&!P) out.setPrioridad("4");
				else if(B&&!G&&!P)out.setPrioridad("1");
				else if(!B&&G&&!P)out.setPrioridad("2");
				else out.setPrioridad("3");
			}
		}			
		return out;
	}	
	
	private OutDto getLastMoveET (String numCia, String numEmp, String prioridad) {
		String servicio = "DataBase/EtChecada/getLastChecada/{idCia}/{idEmp}";
		OutDto out = new OutDto();
		
		Map<String, Long> vars = new HashMap<>();
		vars.put("idCia", Long.parseLong(numCia));
		vars.put("idEmp", Long.parseLong(numEmp));
		
		EtChecadaDto lastMove = template.getForObject(url2+servicio, EtChecadaDto.class, vars);
		if(lastMove != null){		
			out.setError(false);
			out.setCodigo("ET000");
			if(lastMove.getId()!=null) {
				out.setHoraChecada(lastMove.getId().getFecha().toString());
				out.setTipoChecada(lastMove.getId().getTipo());
			}else {
				out.setCodigo(codigoError);
			}
		}else {
			out.setCodigo("ET000");
		}
			
		if(prioridad!=null) {
			if(prioridad.equalsIgnoreCase("true")) {	
				servicio = "DataBase/EtEmpEst/getEstaciones/{ciaNum}/{empNum}";
				Map<String, Long> estacionesVar = new HashMap<>();
				estacionesVar.put("ciaNum", Long.parseLong(numCia));
				estacionesVar.put("empNum", Long.parseLong(numEmp));
				ListEtEmpEstDto estaciones = template.getForObject(url2+servicio, ListEtEmpEstDto.class, estacionesVar);
				Boolean UUID = false;
				Boolean lyl  = false;
				
				if(estaciones!=null) {
					List <EtEmpEstDto> responseList = estaciones.getEtEmpEsts();
					for(EtEmpEstDto x : responseList) {
						if(x.getEtEstacion().getLatitud()!=null && x.getEtEstacion().getLatitud()!=null&&
						   x.getEtEstacion().getRango()!=null) lyl = true;
						
						if(x.getEtEstacion().getUuid()!=null) UUID = true;
					}
				}
				
				if(UUID && !lyl)out.setPrioridad("1");
				if(UUID && lyl)out.setPrioridad("3");
				if(!UUID && lyl)out.setPrioridad("2");
			}
		}	
		return out;
	}
}