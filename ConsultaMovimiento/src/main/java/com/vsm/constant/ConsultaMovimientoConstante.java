package com.vsm.constant;

public interface ConsultaMovimientoConstante {
	public static final String SERVICIO_DB_HUMAN = System.getenv().get("GET_SELECT_DB");
	public static final String SERVICIO_DB_ETIME = System.getenv().get("GET_SELECT_DB_ETIME");
	public static final String CONSULTA_AMBIENTE_SERVICE = System.getenv().get("GET_CONSULTA_AMBIENTE");
	
	//Errores Constantes
	public static final String ERROR_GENERAL = System.getenv().get("GET_COD_ERROR_EG000");
	public static final String DESCRIPCION_ERROR_GENERAL = System.getenv().get("GET_DESC_ERROR_EG000");
	public static final String SUCCES = System.getenv().get("GET_COD_ERROR_ET000");
	public static final String COMPANIA_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET004");
	public static final String EMPLEADO_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET005");
	public static final String COMPANIA_NULA = System.getenv().get("GET_COD_ERROR_ET001");
	public static final String EMPLEADO_NULO = System.getenv().get("GET_COD_ERROR_ET002");
}