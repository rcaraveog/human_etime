package com.vsm.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RnDto{
	private RnPKDto id;
	private String rnArea;
	private String rnCausaRetardo;
	private String rnCentro;
	private String rnChequeo;
	private BigDecimal rnComida;
	private String rnEstacion;
	private LocalDateTime rnFecModif;
	private BigDecimal rnGafete;
	private LocalDateTime rnHorEnt;
	private LocalDateTime rnHorSal;
	private LocalDateTime rnHoraModif;
	private String rnLinea;
	private String rnSts;
	private String rnUsuario;
	private BigDecimal rnZona;
}