package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InDto {
	String numeroCompania;
	String empleado;
	String consultaPrioridad;
}