package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RnPKDto{
	private long rnCia;
	private long rnNumEmp;
	private String rnFecha;
	private long rnSecuencia;
}