package com.vsm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.vsm.constant.RegistraChecadaConstante;
import com.vsm.dto.CompareRequest;
import com.vsm.dto.CompareResponse;
import com.vsm.dto.EtChecadaDto;
import com.vsm.dto.EtChecadaInsDto;
import com.vsm.dto.EtChecadaInsPKDto;
import com.vsm.dto.EtCiaDto;
import com.vsm.dto.EtEmpEstDto;
import com.vsm.dto.EtParametroDto;
import com.vsm.dto.EtUnidadMedidaDto;
import com.vsm.dto.EtUsuarioDto;
import com.vsm.dto.InDto;
import com.vsm.dto.ListEtEmpEstDto;
import com.vsm.dto.OutDto;
import com.vsm.dto.ValidaEstacionDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*") 
@RestController()
@RequestMapping("/registraChecada")
@Api(tags = "RegChecadaET")
public class RegistraChecadaController {
	@Autowired
	RestTemplate template;
	
	@ApiOperation(value = "Registro de Checadas Human eLand Desvinculado")
	@PostMapping(value="/saveDesvinculado", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public OutDto getAmbinteByCia (@RequestBody InDto in){
		OutDto out = new OutDto();
		int idChecada = 1;
		
		String servicio = RegistraChecadaConstante.COMPANIA_ET_SERVICE;
		Map<String, Long> numCiaParam = new HashMap<>();
		numCiaParam.put("idCia", Long.parseLong(in.getNumeroCompania()));
		EtCiaDto cia = template.getForObject(servicio, EtCiaDto.class, numCiaParam);
		
		if(cia!=null) {
			servicio = RegistraChecadaConstante.EMPLEADO_ET_SERVICE;
			Map<String, Long> getEmpParam = new HashMap<>();
			getEmpParam.put("ciaNum", cia.getIdCia());
			getEmpParam.put("empNum", Long.parseLong(in.getEmpleado()));
			EtUsuarioDto emp = template.getForObject(servicio, EtUsuarioDto.class, getEmpParam);
			
			if(emp != null) {
				servicio = RegistraChecadaConstante.EMPLEADO_ADM_ET_SERVICE;
				Map<String, Long> getAdmParam = new HashMap<>();
				getAdmParam.put("idCia", cia.getIdCia());
				EtUsuarioDto adm = template.getForObject(servicio, EtUsuarioDto.class, getAdmParam);
				
				if(adm != null) {
					servicio = RegistraChecadaConstante.CHECADA_ET_SERVICE;
					Map<String, String> getChecadaParam = new HashMap<>();
					getChecadaParam.put("idCia", in.getNumeroCompania());
					getChecadaParam.put("idEmp", in.getEmpleado());
					getChecadaParam.put("fechaIni", in.getFechaHoraChecada().substring(0, 11) + "00:00:00");
					getChecadaParam.put("fechFin", in.getFechaHoraChecada().substring(0, 19));
					EtChecadaDto checada = template.getForObject(servicio, EtChecadaDto.class, getChecadaParam);
					
					if(checada == null && !in.getTipoChecada().equalsIgnoreCase("E")) { 
						out.setError(true);
						out.setCodigo(RegistraChecadaConstante.ENTRADA_NO_EXISTE);
						return out;
					}
					
					if(checada != null && in.getTipoChecada().equalsIgnoreCase("E") 
					   && checada.getId().getTipo().equalsIgnoreCase("E")) { 
						out.setError(true);
						out.setCodigo(RegistraChecadaConstante.ENTRADA_DUPLICADA);
						return out;
					}
					
					if(checada != null && in.getTipoChecada().equalsIgnoreCase("S") 
					   && checada.getId().getTipo().equalsIgnoreCase("S")) { 
						out.setError(true);
						out.setCodigo(RegistraChecadaConstante.SALIDA_DUPLICADA);
						return out;
					}
					
					if(checada != null && in.getFechaHoraChecada().replace("T", " ").equalsIgnoreCase(
					   checada.getId().getFecha())) { 
						out.setError(true);
						out.setCodigo(RegistraChecadaConstante.REGISTRO_DUPLICADO);
						return out;
					}
					
					if(checada != null) {
						idChecada = (int) checada.getId().getIdChecada() + 1;
					}
					
					ValidaEstacionDto valEst = this.valEstacion(in);
					if(valEst.getCodigo()==RegistraChecadaConstante.SUCCES) {
						Map<String, String> mapfoto = this.GetFoto(Long.parseLong(in.getNumeroCompania()), Long.parseLong(in.getEmpleado()));
						servicio = RegistraChecadaConstante.COMPARA_FOTO_ET_SERVICE;
						CompareRequest req =  new CompareRequest();
						req.setCompany(in.getNumeroCompania());
						req.setEmployeeNo(in.getEmpleado());
						req.setModel(mapfoto.get("foto"));
						req.setPhoto(in.getFoto());
						req.setHash(encryptPassword(mapfoto.get("foto")));
						
						CompareResponse res = template.postForObject(servicio, req, CompareResponse.class);
						
						if(res.isRes()) {
							servicio = RegistraChecadaConstante.REGISTRA_CHECADA_ET_SERVICE;
							EtChecadaInsDto regChecada = new EtChecadaInsDto();
							EtChecadaInsPKDto regChecadaPk = new EtChecadaInsPKDto();
							
							regChecadaPk.setIdChecada(idChecada);
							regChecadaPk.setIdCia(cia.getIdCia());
							regChecadaPk.setIdEmpleado(Long.parseLong(in.getEmpleado()));
							regChecadaPk.setIdEstacion(Long.valueOf(valEst.getIdEstacion()));
							regChecadaPk.setTipo(in.getTipoChecada());
							regChecadaPk.setFecha(LocalDateTime.parse(in.getFechaHoraChecada().substring(0, 19)));
							
							regChecada.setEstatus("A");
							regChecada.setId(regChecadaPk);
							
							String guardarContacto = template.postForObject(servicio, regChecada, String.class);
							
							if(guardarContacto.equalsIgnoreCase("true")) {
								out.setError(false);
								out.setCodigo(RegistraChecadaConstante.SUCCES);
								out.setEstacion(valEst.getIdEstacion());
							}else {
								out.setError(true);
								out.setCodigo(RegistraChecadaConstante.ERROR_BD_INSERT);
								return out;
							}
						}else {
							out.setError(true);
							out.setCodigo(RegistraChecadaConstante.ERROR_COMPARA_FOTO);
							return out;
						}
					}else {
						out.setError(true);
						out.setCodigo(valEst.getCodigo());
						return out;
					}
				}else {
					out.setError(true);
					out.setCodigo(RegistraChecadaConstante.ADMINISTRADOR_NO_EXISTE);
					return out;
				}
			}else {
				out.setError(true);
				out.setCodigo(RegistraChecadaConstante.EMPLEADO_NO_EXISTE);
				return out;
			}
		}else {
			out.setError(true);
			out.setCodigo(RegistraChecadaConstante.COMPANIA_NO_EXISTE);
			return out;
		}
		
		return out;
	}
	
	private ValidaEstacionDto valEstacion (InDto in) {
		ValidaEstacionDto out = new ValidaEstacionDto();
		String servicio = RegistraChecadaConstante.ESTACION_ET_SERVICE;
		Map<String, Long> estacionesVar = new HashMap<>();
		estacionesVar.put("ciaNum", Long.parseLong(in.getNumeroCompania()));
		estacionesVar.put("empNum", Long.parseLong(in.getEmpleado()));
		double  metros = 0;
		
		ListEtEmpEstDto estaciones = template.getForObject(servicio, ListEtEmpEstDto.class, estacionesVar);
				
		if(estaciones!=null&&estaciones.getEtEmpEsts().size()>0) {
			List <EtEmpEstDto> responseList = estaciones.getEtEmpEsts();
			if(in.getPrioridad().equalsIgnoreCase("2")||in.getPrioridad().equalsIgnoreCase("3")) {				
				for(EtEmpEstDto x : responseList) {
					try{     
				        double radioTierra = 6371;//en kilómetros
				        double  Latitud01  =x.getEtEstacion().getLatitud().doubleValue();   
				        double  Latitud02  =Double.parseDouble(in.getsGeolocalizacion().getLatitud());   
				        double  Longitud01 =x.getEtEstacion().getLongitud().doubleValue();    
				        double  Longitud02 =Double.parseDouble(in.getsGeolocalizacion().getLongitud());
				        double dLat = Math.toRadians(Latitud02 - Latitud01);      
				        double dLng = Math.toRadians(Longitud02 - Longitud01);
				        double sindLat = Math.sin(dLat / 2);      
				        double sindLng = Math.sin(dLng / 2);      
				        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(Latitud01)) * Math.cos(Math.toRadians(Latitud02)); 
				        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));      
				        double distancia = radioTierra * va2;      
				        
				        metros = 1000 * distancia;        
				     
					}catch ( Exception ex ){           
						out.setCodigo(RegistraChecadaConstante.ERROR_GENERAL);
						return out;           
					}
					
					servicio = RegistraChecadaConstante.UNIDAD_MEDIDA_ET_SERVICE;
					Map<String, String> uMedidaVar = new HashMap<>();
					uMedidaVar.put("cveUnidad",x.getEtEstacion().getCveUnidad());
					EtUnidadMedidaDto uMed = template.getForObject(servicio, EtUnidadMedidaDto.class, uMedidaVar);
					
					if(uMed!=null) {
						double rango = uMed.getValorMetros().doubleValue() * x.getEtEstacion().getRango().doubleValue();
						if(rango>=metros) {
							out.setCodigo(RegistraChecadaConstante.SUCCES);
							out.setIdEstacion(String.valueOf(x.getEtEstacion().getIdEstacion()));
							out.setNombreEstacion(x.getEtEstacion().getNombre());
							return out;
						}
					}else {
						out.setCodigo(RegistraChecadaConstante.UNIDAD_MEDIDA_NO_EXISTE);
						return out;
					}
				}
			}
			
			if(in.getPrioridad().equalsIgnoreCase("1")||in.getPrioridad().equalsIgnoreCase("3")) {
				String beaconIn = in.getsBeacons().getBeacons().get(0).get("beacon");
				for(EtEmpEstDto x : responseList) {
					if(x.getEtEstacion().getUuid()==beaconIn) {
						out.setCodigo(RegistraChecadaConstante.SUCCES);
						out.setIdEstacion(String.valueOf(x.getEtEstacion().getIdEstacion()));
						out.setNombreEstacion(x.getEtEstacion().getNombre());
						return out;
					}
				}
			}
		}else {
			out.setCodigo(RegistraChecadaConstante.ESTACION_NO_EXISTE);
			return out;
		}
		
		if(in.getPrioridad().equalsIgnoreCase("1")) {
			out.setCodigo(RegistraChecadaConstante.P1_MATCH_NO_EXISTE);
		}
		
		if(in.getPrioridad().equalsIgnoreCase("2")) {
			out.setCodigo(RegistraChecadaConstante.P2_MATCH_NO_EXISTE);
		}
		
		if(in.getPrioridad().equalsIgnoreCase("3")) {
			out.setCodigo(RegistraChecadaConstante.P3_MATCH_NO_EXISTE);
		}
		
		return out;
	}
	
	public Map<String,String> GetFoto(long numCia, long numEmp) {
		String servicio = RegistraChecadaConstante.PARAMETRO_ET_SERVICE;
		Formatter fmtCia = new Formatter();
		fmtCia.format("%05d",numCia);
		
		Formatter fmtEmp = new Formatter();
		fmtEmp.format("%010d",numEmp);
		
		EtParametroDto[] respon1 = template.getForObject(servicio, EtParametroDto[].class, "FileHumaneTime");

		List<EtParametroDto> listGral = new ArrayList<EtParametroDto>();
		listGral = Arrays.asList(respon1);

		String valorAlfanumerico = null;

		for (int i = 0; i < listGral.size(); i++) {
			EtParametroDto e = listGral.get(i);
			valorAlfanumerico = e.getValor();
		}

		String filePath = valorAlfanumerico + "\\" + fmtCia + "\\" + fmtEmp	+ ".JPG";
		fmtCia.close();
		fmtEmp.close();
		filePath = filePath.replace("\\", "/");
		filePath = filePath.replace("/", File.separator);

		File file = new File(filePath);
		String lastModified = String.valueOf(file.lastModified());
		Map<String, String> map = new HashMap<String, String>();
		map.put("fechaFoto", lastModified);
		
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			// Reading a file from file system
			byte fileData[] = new byte[(int) file.length()];
			imageInFile.read(fileData);
			map.put("foto",Base64.getEncoder().encodeToString(fileData));
		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
			map.put("foto","File not found" + e);
			return map;
		} catch (IOException ioe) {
			System.out.println("Exception while reading the file " + ioe);
			map.put("foto","Exception while reading the file " + ioe);
			return map;
		}
		
		return map;
	}
	
	private String encryptPassword(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] enc = md.digest();
			String md5Sum = Base64.getEncoder().encodeToString(enc);
			return md5Sum;
		} catch (NoSuchAlgorithmException nsae) {
			System.out.println(nsae.getMessage());
			return null;
		}
	}
}