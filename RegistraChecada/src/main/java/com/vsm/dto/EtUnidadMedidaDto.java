package com.vsm.dto;

import java.math.BigDecimal;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtUnidadMedidaDto {
	private String cveUnidad;
	private String descripcion;
	private String estatus;
	private BigDecimal valorMetros;
}