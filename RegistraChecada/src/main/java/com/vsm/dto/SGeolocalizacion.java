package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SGeolocalizacion {
	String latitud;
	String longitud;
}