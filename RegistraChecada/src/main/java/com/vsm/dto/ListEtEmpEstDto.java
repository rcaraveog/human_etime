package com.vsm.dto;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ListEtEmpEstDto {
	private List<EtEmpEstDto> EtEmpEsts;
}