package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ValidaEstacionDto {
	String codigo;
	String idEstacion;
	String nombreEstacion;
}