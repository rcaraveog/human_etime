package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtChecadaPKDto {
	private long idChecada;
	private long idCia;
	private long idEmpleado;
	private long idEstacion;
	private String fecha;
	private String tipo;
}