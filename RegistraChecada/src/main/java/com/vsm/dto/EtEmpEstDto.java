package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtEmpEstDto {
	private EtEmpEstPKDto id;
	private EtEstacionDto etEstacion;
}