package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtChecadaDto {
	private EtChecadaPKDto id;
	private String estatus;
}