package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtChecadaInsDto {
	private EtChecadaInsPKDto id;
	private String estatus;
}