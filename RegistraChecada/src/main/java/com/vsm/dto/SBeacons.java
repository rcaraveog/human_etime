package com.vsm.dto;

import java.util.List;
import java.util.Map;	
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SBeacons {
	List<Map <String,String>> beacons;
}