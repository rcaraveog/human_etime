package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OutDto {
	boolean isError;
	String  errorMessage;
	
	String codigo;
	String estacion;
}