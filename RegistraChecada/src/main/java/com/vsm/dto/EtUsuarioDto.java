package com.vsm.dto;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtUsuarioDto {
	private long idUsuario;
	private String apMaterno;
	private String apPaterno;
	private String aviso;
	private String codTelefono;
	private String correo;
	private String estatus;
	private BigDecimal idRol;
	private String nombre;
	private String password;
	private BigDecimal telefono;
}