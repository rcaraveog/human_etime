package com.vsm.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompareRequest {
	String company;
	String employeeNo;
	String hash;
	String model;
	String photo;
}