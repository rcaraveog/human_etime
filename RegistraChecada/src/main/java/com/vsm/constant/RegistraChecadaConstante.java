package com.vsm.constant;

public interface RegistraChecadaConstante {
	public static final String COMPANIA_ET_SERVICE = System.getenv().get("GET_CIA_ET_BY_NUMBER");
	public static final String EMPLEADO_ET_SERVICE = System.getenv().get("GET_EMP_ET_BY_NUMBER");
	public static final String EMPLEADO_ADM_ET_SERVICE = System.getenv().get("GET_ADM_ET_BY_CIA");
	public static final String CHECADA_ET_SERVICE = System.getenv().get("GET_CHECADA_ET_BY_DATE");
	public static final String ESTACION_ET_SERVICE = System.getenv().get("GET_ESTACION_ET");
	public static final String UNIDAD_MEDIDA_ET_SERVICE = System.getenv().get("GET_UNIDAD_MEDIDA_ET");
	public static final String PARAMETRO_ET_SERVICE = System.getenv().get("GET_PARAMETRO_ET");
	public static final String COMPARA_FOTO_ET_SERVICE = System.getenv().get("COMPARA_FOTO_REST_SERVICE");
	public static final String REGISTRA_CHECADA_ET_SERVICE = System.getenv().get("REG_CHECADA_ET_BY_DATE");
	
	//Errores Constantes
	public static final String ERROR_GENERAL = System.getenv().get("GET_COD_ERROR_EG000");
	public static final String ERROR_BD_INSERT = System.getenv().get("GET_COD_ERROR_BD409");
	public static final String SUCCES = System.getenv().get("GET_COD_ERROR_ET000");
	public static final String COMPANIA_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET313");
	public static final String EMPLEADO_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET314");
	public static final String ADMINISTRADOR_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET239");
	public static final String ENTRADA_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET505");
	public static final String ENTRADA_DUPLICADA = System.getenv().get("GET_COD_ERROR_ET033");
	public static final String SALIDA_DUPLICADA = System.getenv().get("GET_COD_ERROR_ET040");
	public static final String REGISTRO_DUPLICADO = System.getenv().get("GET_COD_ERROR_ET042");
	public static final String ESTACION_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET506");
	public static final String UNIDAD_MEDIDA_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET410");
	public static final String P1_MATCH_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET029");
	public static final String P2_MATCH_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET030");
	public static final String P3_MATCH_NO_EXISTE = System.getenv().get("GET_COD_ERROR_ET507");
	public static final String ERROR_COMPARA_FOTO = System.getenv().get("GET_COD_ERROR_ET032");
}